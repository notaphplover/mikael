﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Mikael.automata.finite;
using Mikael.grammar;
using Mikael.grammar.rule;
using Mikael.grammar.symbol;

namespace MikaelTest.automata.finite
{
    [TestClass]
    public class FiniteAutomataProviderTest
    {
        /// <summary>
        /// Create a new instance of this class
        /// </summary>
        public FiniteAutomataProviderTest()
        { }

        [TestMethod]
        public void FiniteAutomataMustProvideAutomatasFromGrammars()
        {
            FiniteAutomataProvider<FiniteAutomata<Symbol>, Symbol> automataProvider = new FiniteAutomataProvider<FiniteAutomata<Symbol>, Symbol>();

            G3RightGrammar<Symbol> g3RightGrammar = new G3RightGrammar<Symbol>(
                initialSymbol: new Symbol("S"), 
                rules: new G3RightRule<Symbol>[] {
                    new G3RightRule<Symbol>(new Symbol("S"), new G3RightSymbols<Symbol>(new Symbol("1"), new Symbol("a"))),
                    new G3RightRule<Symbol>(new Symbol("1"), new G3RightSymbols<Symbol>(new Symbol("1"), new Symbol("a"))),
                    new G3RightRule<Symbol>(new Symbol("1"), new G3RightSymbols<Symbol>(new Symbol("2"), new Symbol("b"))),
                    new G3RightRule<Symbol>(new Symbol("2"), new G3RightSymbols<Symbol>(new Symbol("2"), new Symbol("b"))),
                    new G3RightRule<Symbol>(new Symbol("2"), new G3RightSymbols<Symbol>(new Symbol(Symbol.LAMBDA_ALIAS), new Symbol(Symbol.LAMBDA_ALIAS)))
                }, 
                nonTerminalSymbols: new Symbol[] { new Symbol("S"), new Symbol("1"), new Symbol("2"), new Symbol("3") }, 
                terminalSymbols: new Symbol[] { new Symbol("a"), new Symbol("b") }
            );

            FiniteAutomata<Symbol> automata = 
                automataProvider.Provide<G3RightGrammar<Symbol>, G3RightRule<Symbol>, G3RightSymbols<Symbol>>(
                    g3RightGrammar, new Symbol(Symbol.LAMBDA_ALIAS)
                )
                .ToDeterministicAutomata()
            ;

            Assert.IsNotNull(automata);

            Symbol[] word = new Symbol[] { new Symbol("a"), new Symbol("a"), new Symbol("b"), new Symbol("b") };

            for (int i = 0; i < word.Length; ++i)
                Assert.IsTrue(automata.TryDeterministicProcess(word[i]));

            Assert.IsTrue(automata.IsInFinalState());
        }
    }
}

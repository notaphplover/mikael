﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Mikael.automata.finite;
using Mikael.datastruct;
using Mikael.grammar.symbol;
using MikaelTest.util;
using System.Collections.Generic;

namespace MikaelTest.automata.finite
{
    [TestClass]
    public class FiniteAutomataTest
    {
        /// <summary>
        /// Create a new instance of this class
        /// </summary>
        public FiniteAutomataTest()
        { }

        /// <summary>
        /// Analyzes the states of an automata and returns if that one is deterministic
        /// </summary>
        /// <typeparam name="TSymbol">Type of the symbols of the automata</typeparam>
        /// <param name="automata">True if the automata is deterministic</param>
        /// <returns>True if the automata is deterministic</returns>
        protected bool IsDeterministic<TAutomata, TSymbol, TDictionary>(FiniteAutomata<TAutomata, TSymbol, TDictionary> automata)
            where TAutomata : FiniteAutomata<TAutomata, TSymbol, TDictionary>, new()
            where TSymbol : ISymbol<TSymbol>
            where TDictionary : IDictionary<TSymbol, MikaelSet<int>>, new()
        {
            for (int i = 0; i < automata.GetStates(); ++i)
                foreach (KeyValuePair<TSymbol, MikaelSet<int>> transition in automata.GetState(i))
                    if (transition.Key.Equals(Symbol.LAMBDA_ALIAS) || transition.Value.Count != 1)
                        return false;
                
            return true;
        }

        [TestMethod]
        public void FiniteAutomataMustBeInitializable()
        {
            new FiniteAutomata<Symbol>(0, new int[] { }, new Symbol(Symbol.LAMBDA_ALIAS));
        }

        [TestMethod]
        public void FiniteAutomataMustBeAbleToCreateDeterministicAutomata()
        {
            Symbol lambdaSymbol = new Symbol(Symbol.LAMBDA_ALIAS);
            FiniteAutomata<Symbol> automata = new FiniteAutomata<Symbol>(0, new int[] { 2 }, lambdaSymbol);

            //State 0
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 4 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("a"), 
                    new int[] { 1, 2, 3 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("b"),
                    new int[] { 0 }
                ),
            });

            //State 1
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("d"),
                    new int[] { 0 }
                ),
            });

            //State 2
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] { });

            //State 3
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 1 }
                ),
            });

            //State 4
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 3 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("c"),
                    new int[] { 3 }
                ),
            });

            SortedList<int, ICollection<int>> finalStatesTrace;
            FiniteAutomata<Symbol> deterministicAutomata = automata.ToDeterministicAutomata(out finalStatesTrace);
            Assert.IsTrue(IsDeterministic(deterministicAutomata));
        }

        [TestMethod]
        public void FiniteAutomataMustBeAbleToMinimizeIfDeterministic()
        {
            Symbol lambdaSymbol = new Symbol(Symbol.LAMBDA_ALIAS);
            FiniteAutomata<Symbol> automata = new FiniteAutomata<Symbol>(0, new int[] { 4 }, lambdaSymbol);

            //State 0
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("a"),
                    new int[] { 1 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("b"),
                    new int[] { 2 }
                ),
            });

            //State 1
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("a"),
                    new int[] { 1 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("b"),
                    new int[] { 3 }
                ),
            });

            //State 2
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("a"),
                    new int[] { 1 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("b"),
                    new int[] { 2 }
                ),
            });

            //State 3
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("a"),
                    new int[] { 1 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("b"),
                    new int[] { 4 }
                ),
            });

            //State 4
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("a"),
                    new int[] { 1 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("b"),
                    new int[] { 2 }
                ),
            });

            FiniteAutomata<Symbol> minimizedAutomata = automata.MinimizedAutomata();
            Assert.IsNotNull(minimizedAutomata);

            WordGenerator<Symbol> wordGenerator = new WordGenerator<Symbol>(new Symbol[] { new Symbol("a"), new Symbol("b") });

            ICollection<Symbol[]> testWords = wordGenerator.GenerateSet(6);

            foreach (Symbol[] word in testWords) {
                automata.CurrentState = automata.InitialState;
                minimizedAutomata.CurrentState = minimizedAutomata.InitialState;
                foreach (Symbol symbol in word)
                    Assert.AreEqual<bool>(automata.TryDeterministicProcess(symbol), minimizedAutomata.TryDeterministicProcess(symbol));

                Assert.AreEqual<bool>(automata.IsInFinalState(), minimizedAutomata.IsInFinalState());
            }
        }

        [TestMethod]
        public void FiniteAutomataMustBeAbleToCreateDeterministicAutomataAndProcessInput()
        {
            Symbol lambdaSymbol = new Symbol(Symbol.LAMBDA_ALIAS);
            FiniteAutomata<Symbol> automata = new FiniteAutomata<Symbol>(0, new int[] { 2 }, lambdaSymbol);

            //State 0
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 4 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("a"),
                    new int[] { 1, 2, 3 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("b"),
                    new int[] { 0 }
                ),
            });

            //State 1
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("d"),
                    new int[] { 0 }
                ),
            });

            //State 2
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] { });

            //State 3
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 1 }
                ),
            });

            //State 4
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 3 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("c"),
                    new int[] { 3 }
                ),
            });

            SortedList<int, ICollection<int>> finalStatesTrace;
            FiniteAutomata<Symbol> deterministicAutomata = automata.ToDeterministicAutomata(out finalStatesTrace);
            Assert.IsTrue(IsDeterministic(deterministicAutomata));

            Symbol[] word = new Symbol[] {
                new Symbol("a"),
                new Symbol("d"),
                new Symbol("b"),
                new Symbol("d"),
                new Symbol("d"),
                new Symbol("d"),
                new Symbol("c"),
                new Symbol("d"),
                new Symbol("c"),
                new Symbol("d"),
                new Symbol("b"),
                new Symbol("a"),
            };

            foreach (Symbol symbol in word)
                Assert.IsTrue(deterministicAutomata.TryDeterministicProcess(symbol));

            Assert.IsTrue(deterministicAutomata.IsInFinalState());
        }

        [TestMethod]
        public void FiniteAutomataMustBeAbleToRemoveDeadStates()
        {
            //TODO: Implement me!
            Symbol lambdaSymbol = new Symbol(Symbol.LAMBDA_ALIAS);
            FiniteAutomata<Symbol> automata = new FiniteAutomata<Symbol>(0, new int[] { 2 }, lambdaSymbol);

            //State 0
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 4 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("a"),
                    new int[] { 1, 2, 3 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("b"),
                    new int[] { 0 }
                ),
            });

            //State 1
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("d"),
                    new int[] { 0 }
                ),
            });

            //State 2
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] { });

            //State 3
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 1 }
                ),
            });

            //State 4
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 3 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("c"),
                    new int[] { 3 }
                ),
            });

            SortedList<int, ICollection<int>> finalStatesTrace;
            FiniteAutomata<Symbol> deterministicAutomata = automata.ToDeterministicAutomata(out finalStatesTrace).WithoutDeadStates();
            Assert.IsTrue(IsDeterministic(deterministicAutomata));

            Symbol[] word = new Symbol[] {
                new Symbol("a"),
                new Symbol("d"),
                new Symbol("b"),
                new Symbol("d"),
                new Symbol("d"),
                new Symbol("d"),
                new Symbol("c"),
                new Symbol("d"),
                new Symbol("c"),
                new Symbol("d"),
                new Symbol("b"),
                new Symbol("a"),
            };

            foreach (Symbol symbol in word)
                Assert.IsTrue(deterministicAutomata.TryDeterministicProcess(symbol));

            Assert.IsTrue(deterministicAutomata.IsInFinalState());
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Mikael.automata.finite;
using Mikael.grammar.symbol;
using MikaelTest.util;
using System.Collections.Generic;

namespace MikaelTest.automata.finite
{
    [TestClass]
    public class OptimalFiniteAutomataTest
    {
        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        public OptimalFiniteAutomataTest()
        { }

        [TestMethod]
        public void OptimalFiniteAutomataMustBeEquivalentToItsSourceAutomata()
        {
            Symbol lambdaSymbol = new Symbol(Symbol.LAMBDA_ALIAS);
            FiniteAutomata<Symbol> automata = new FiniteAutomata<Symbol>(0, new int[] { 2 }, lambdaSymbol);

            //State 0
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 4 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("a"),
                    new int[] { 1, 2, 3 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("b"),
                    new int[] { 0 }
                ),
            });

            //State 1
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("d"),
                    new int[] { 0 }
                ),
            });

            //State 2
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] { });

            //State 3
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 1 }
                ),
            });

            //State 4
            automata.AddState(new KeyValuePair<Symbol, ICollection<int>>[] {
                new KeyValuePair<Symbol, ICollection<int>>(
                    lambdaSymbol,
                    new int[] { 3 }
                ),
                new KeyValuePair<Symbol, ICollection<int>>(
                    new Symbol("c"),
                    new int[] { 3 }
                ),
            });

            SortedList<int, ICollection<int>> finalStatesTrace;
            FiniteAutomata<Symbol> deterministicAutomata = automata.ToDeterministicAutomata(out finalStatesTrace);

            Assert.IsNotNull(deterministicAutomata);

            OptimalFiniteAutomata<Symbol> optimalAutomata = automata.GenerateOptimalAutomata();

            Assert.IsNotNull(optimalAutomata);

            WordGenerator<Symbol> wordGenerator = new WordGenerator<Symbol>(new Symbol[] { new Symbol("a"), new Symbol("b"), new Symbol("c"), new Symbol("d") });

            ICollection<Symbol[]> testWords = wordGenerator.GenerateSet(6);

            foreach (Symbol[] word in testWords) {
                deterministicAutomata.CurrentState = deterministicAutomata.InitialState;
                optimalAutomata.CurrentState = optimalAutomata.InitialState;
                foreach (Symbol symbol in word)
                    Assert.AreEqual<bool>(deterministicAutomata.TryDeterministicProcess(symbol), optimalAutomata.TryProcess(symbol));

                Assert.AreEqual<bool>(deterministicAutomata.IsInFinalState(), optimalAutomata.IsInFinalState());
            }
        }
    }
}

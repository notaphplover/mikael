﻿using Mikael.datastruct;
using Mikael.grammar.symbol;
using System;
using System.Collections.Generic;

namespace MikaelTest.util
{
    /// <summary>
    /// Represents a word generator.
    /// </summary>
    /// <typeparam name="TSymbol">Type of the symbols to use</typeparam>
    public class WordGenerator<TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        /// <summary>
        /// Set of terminal symbols of the associated grammar.
        /// </summary>
        protected TSymbol[] _allowedSymbols;

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="symbols">Set of terminal symbols of the associated grammar.</param>
        public WordGenerator(ICollection<TSymbol> symbols)
        {
            _allowedSymbols = new TSymbol[symbols.Count];

            int i = 0;
            foreach (TSymbol symbol in symbols)
                _allowedSymbols[i++] = symbol;
        }

        /// <summary>
        /// Generates a set of words
        /// </summary>
        /// <param name="length">Length of the set.</param>
        /// <returns>Collection of generated words</returns>
        public ICollection<TSymbol[]> GenerateSet(int length)
        {
            TSymbol[][] words = new TSymbol[(int)Math.Pow(_allowedSymbols.Length, length)][];

            int[] counters = new int[length];

            //Unnecesary, but I´ll include this for
            for (int i = 0; i < counters.Length; ++i)
                counters[i] = 0;

            for (int i = 0; i < words.Length; ++i) {
                words[i] = new TSymbol[length];
                for (int j = 0; j < length; ++j)
                    words[i][j] = _allowedSymbols[counters[j]];
            }

            return words;
        }

        /// <summary>
        /// Increments the counters of the word.
        /// </summary>
        /// <param name="counters">Counters used to generate a set of words</param>
        /// <param name="length">Length of the set</param>
        private static void IncrementCounters(int[] counters, int length)
        {
            for (int i = counters.Length - 1; i >= 0; --i) {
                counters[i] = (counters[i] + 1) % length;

                if (counters[i] != 0)
                    break;
            }
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Mikael.lexical;

using System;
using System.IO;
using System.Text;

namespace MikaelTest.lexical
{
    [TestClass]
    public class MikaelStreamReaderTest
    {
        protected const string PATH_FILE_TEST_1 = "resources/lexical/sample-text-1.txt";

        /// <summary>
        /// Create a new instance of this class
        /// </summary>
        public MikaelStreamReaderTest()
        { }

        [TestMethod]
        public void MikaelStreamReaderMustBeInitializable()
        {
            using (FileStream fileStream = new FileStream(PATH_FILE_TEST_1, FileMode.Open)) {
                new MikaelStreamReader(fileStream, Encoding.Unicode).Dispose();
            }
        }

        [TestMethod]
        public void MikaelStreamReaderMustReadCharacters()
        {
            StringBuilder sb = new StringBuilder();
            
            using (FileStream fileStream = new FileStream(PATH_FILE_TEST_1, FileMode.Open)) {
                using (MikaelStreamReader sr = new MikaelStreamReader(fileStream, Encoding.Unicode)) {
                    char charRead;
                    int i = 0;

                    while (sr.ReadCharacter(out charRead)) {
                        sb.Append(charRead);
                        Assert.AreEqual(0, sr.StartIndex);
                        Assert.AreEqual(++i, sr.EndIndex);
                    } 
                }
            }

            Assert.AreEqual(File.ReadAllText(PATH_FILE_TEST_1), sb.ToString());
        }

        [TestMethod]
        public void MikaelStreamReaderMustReadCharacterBlocks()
        {
            int blockSize = 8;
            StringBuilder sb = new StringBuilder();

            using (FileStream fileStream = new FileStream(PATH_FILE_TEST_1, FileMode.Open)) {
                using (MikaelStreamReader sr = new MikaelStreamReader(fileStream, Encoding.Unicode)) {
                    char[] buffer = new char[blockSize];
                    int i = 0;
                    int charactersRead;

                    do {
                        charactersRead = sr.ReadCharacters(buffer, 0, blockSize);
                        sb.Append(new string(buffer, 0, charactersRead));
                        Assert.AreEqual(0, sr.StartIndex);
                        Assert.AreEqual(i += charactersRead, sr.EndIndex);
                    } while (charactersRead == blockSize);
                }  
            }

            Assert.AreEqual(File.ReadAllText(PATH_FILE_TEST_1), sb.ToString());
        }

        [TestMethod]
        public void MikaelStreamReaderMustMoveItsPointers()
        {
            int blockSize = 20;
            string substring;
            using (FileStream fileStream = new FileStream(PATH_FILE_TEST_1, FileMode.Open)) {
                using (MikaelStreamReader sr = new MikaelStreamReader(fileStream, Encoding.Unicode)) {
                    Assert.ThrowsException<InvalidOperationException>(() => { sr.StartIndex = -1; });
                    sr.ReadCharacters(new char[10], 0, 10);
                    sr.StartIndex = 1;
                    Assert.AreEqual(1, sr.StartIndex);
                    sr.EndIndex = 4;
                    Assert.AreEqual(4, sr.EndIndex);
                    Assert.ThrowsException<InvalidOperationException>(() => { sr.StartIndex = 5; });
                    sr.EndIndex = blockSize;
                    Assert.AreEqual(blockSize, sr.EndIndex);
                    Assert.ThrowsException<InvalidOperationException>(() => { sr.EndIndex = 0; });
                    sr.StartIndex = 0;
                    Assert.AreEqual(0, sr.StartIndex);
                    substring = new string(sr.getSubstring());
                }
            }

            Assert.AreEqual(File.ReadAllText(PATH_FILE_TEST_1).Substring(0, blockSize), substring);
        }
    }
}

﻿using Mikael.datastruct;
using Mikael.grammar;
using Mikael.grammar.rule;
using Mikael.grammar.symbol;

using System.Collections.Generic;

namespace Mikael.automata.finite
{
    /// <summary>
    /// Represents an entity that is able to generate automatas from other related entities.
    /// </summary>
    /// <typeparam name="TAutomata">Type of the automatas generated</typeparam>
    /// <typeparam name="TSymbol">Type of the symbols used</typeparam>
    /// <typeparam name="TDictionary">Type of the dictionaries used to stored rules</typeparam>
    public class FiniteAutomataProvider<TAutomata, TSymbol, TDictionary>
        where TAutomata : IFiniteAutomata<TAutomata, TSymbol, TDictionary>, new()
        where TSymbol : ISymbol<TSymbol>
        where TDictionary : IDictionary<TSymbol, MikaelSet<int>>, new()
    {
        /// <summary>
        /// Creates a new Finite automata provider
        /// </summary>
        public FiniteAutomataProvider() { }

        #region FROM_GRAMMAR

        /// <summary>
        /// Creates an automata from a grammar
        /// </summary>
        /// <typeparam name="TGrammar">Type of the grammar</typeparam>
        /// <typeparam name="TRule">Type of the rules</typeparam>
        /// <typeparam name="TOutput">Type of the output part of the grammar</typeparam>
        /// <param name="grammar">Source grammar</param>
        /// <param name="lamda">Lambda symbol</param>
        /// <returns>Automata generated from the grammar.</returns>
        public TAutomata Provide<TGrammar, TRule, TOutput>(TGrammar grammar, TSymbol lamda)
            where TGrammar: IRegularGrammar<TRule, TOutput, TSymbol>
            where TRule : IG3RightRule<TRule, TOutput, TSymbol>, new()
            where TOutput : IG3Symbols<TOutput, TSymbol>
        {
            SortedDictionary<TSymbol, int> symbolToState;
            int? finalState;
            List<TDictionary> statesList = this.CreateAutomataStates<TGrammar, TRule, TOutput>(grammar, lamda, out symbolToState, out finalState);

            TAutomata automata = new TAutomata();
            automata.Initialize(
                symbolToState[grammar.InitialSymbol],
                finalState.HasValue ? new int[] { finalState.Value } : new int[] { },
                lamda
            );

            foreach (TDictionary statesTransition in statesList) {
                LinkedList<KeyValuePair<TSymbol, ICollection<int>>> statesTransitionCollection = new LinkedList<KeyValuePair<TSymbol, ICollection<int>>>();

                foreach (KeyValuePair<TSymbol, MikaelSet<int>> symbolTransitions in statesTransition) 
                    statesTransitionCollection.AddLast(new KeyValuePair<TSymbol, ICollection<int>>(symbolTransitions.Key, symbolTransitions.Value));
                
                automata.AddState(statesTransitionCollection);
            }

            return automata;
        }

        /// <summary>
        /// Creates the states of an automata from a set of rules
        /// </summary>
        /// <typeparam name="TRule">Type of the rules</typeparam>
        /// <typeparam name="TOutput">Type of the output part of the grammar</typeparam>
        /// <param name="rules">Rules set.</param>
        /// <param name="lamda">Lambda symbol</param>
        /// <param name="symbolToState">Symbol to state translator</param>
        /// <param name="finalState">Final state</param>
        /// <returns>Set of states</returns>
        private List<TDictionary> CreateAutomataStates<TGrammar, TRule, TOutput>(TGrammar grammar, TSymbol lamda, out SortedDictionary<TSymbol, int> symbolToState, out int? finalState)
            where TGrammar : IRegularGrammar<TRule, TOutput, TSymbol>
            where TRule : IG3RightRule<TRule, TOutput, TSymbol>, new()
            where TOutput : IG3Symbols<TOutput, TSymbol>
        {
            ICollection<TRule> rules = grammar.GetRules();
            finalState = null;
            symbolToState = new SortedDictionary<TSymbol, int>();
            //Generate the states. Each non terminal symbol will be an state
            List<TDictionary> statesList = new List<TDictionary>();

            foreach (TRule rule in rules) {
                TDictionary stateTransitions = this.GetStateTransitions(statesList, symbolToState, rule.Input);
                TSymbol transitionSymbol = rule.Output.TSymbol == null ? lamda : rule.Output.TSymbol;
                TSymbol destinationStateSymbol = rule.Output.NTSymbol;

                if (destinationStateSymbol == null || Symbol.LAMBDA_ALIAS.Equals(destinationStateSymbol.GetSymbol())) {
                    //Transition to the final state
                    if (!finalState.HasValue) {
                        //Create the final state
                        finalState = statesList.Count;
                        statesList.Add(new TDictionary());
                    }

                    this.AddTransition(stateTransitions, transitionSymbol, finalState.Value);
                } else {
                    //Create a new entry in the dictionary if the state does not exist
                    int destinationState;

                    if (!symbolToState.TryGetValue(destinationStateSymbol, out destinationState)) {
                        destinationState = statesList.Count;
                        symbolToState.Add(destinationStateSymbol, destinationState);
                        statesList.Add(new TDictionary());
                    }

                    this.AddTransition(stateTransitions, transitionSymbol, destinationState); 
                }
            }

            //At the end, add the initial state if it is not already in states set.
            this.GetStateTransitions(statesList, symbolToState, grammar.InitialSymbol);

            return statesList;
        }

        /// <summary>
        /// Adds a transition from a state to another state using a symbol
        /// </summary>
        /// <param name="stateTransitions">Set of transitions from the state</param>
        /// <param name="transitionSymbol">Symbol of the transition</param>
        /// <param name="destinationState">Destination state of the transition</param>
        private void AddTransition(TDictionary stateTransitions, TSymbol transitionSymbol, int destinationState)
        {
            MikaelSet<int> symbolDestinations;

            if (!stateTransitions.TryGetValue(transitionSymbol, out symbolDestinations)) {
                symbolDestinations = new MikaelSet<int>();
                stateTransitions.Add(transitionSymbol, symbolDestinations);
            }

            symbolDestinations.Add(destinationState); 
        }

        /// <summary>
        /// Obtains the transitions of an state
        /// </summary>
        /// <param name="statesList">States list</param>
        /// <param name="symbolToState">Symbol to state dictionary</param>
        /// <param name="stateSymbol">Symbol associated to the desired state</param>
        /// <returns>Dictionary of transitions from the desired state</returns>
        private TDictionary GetStateTransitions(List<TDictionary> statesList, SortedDictionary<TSymbol, int> symbolToState, TSymbol stateSymbol)
        {
            int state;
            TDictionary stateTransitions;

            if (symbolToState.TryGetValue(stateSymbol, out state)) {
                stateTransitions = statesList[state];
            } else { 
                state = statesList.Count;
                symbolToState.Add(stateSymbol, state);
                stateTransitions = new TDictionary();
                statesList.Add(stateTransitions);
            }

            return stateTransitions;
        }

        #endregion
    }

    /// <summary>
    /// Represents an entity that is able to generate automatas from other related entities.
    /// </summary>
    /// <typeparam name="TAutomata">Type of the automatas generated</typeparam>
    /// <typeparam name="TSymbol">Type of the symbols used</typeparam>
    public class FiniteAutomataProvider<TAutomata, TSymbol> : FiniteAutomataProvider<TAutomata, TSymbol, SortedDictionary<TSymbol, MikaelSet<int>>>
        where TAutomata : IFiniteAutomata<TAutomata, TSymbol, SortedDictionary<TSymbol, MikaelSet<int>>>, new()
        where TSymbol : ISymbol<TSymbol>
    {
        /// <summary>
        /// Creates a new Finite automata provider
        /// </summary>
        public FiniteAutomataProvider() { }
    }
}

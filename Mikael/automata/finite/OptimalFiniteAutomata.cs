﻿using Mikael.grammar.symbol;
using System;
using System.Collections.Generic;

namespace Mikael.automata.finite
{
    public class OptimalFiniteAutomata<TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        #region ATTRIBUTES 

        /// <summary>
        /// Current state of the automata.
        /// </summary>
        protected int _currentState;

        /// <summary>
        /// Final states of the automata.
        /// </summary>
        protected SortedSet<int> _finalStates;

        /// <summary>
        /// States transitions.
        /// </summary>
        protected List<SortedDictionary<TSymbol, int>> _statesTransitions;

        #endregion

        /// <summary>
        /// Initial state of the automata.
        /// </summary>
        public int InitialState { get; }

        /// <summary>
        /// Current state of the automata
        /// </summary>
        public int CurrentState {
            get => _currentState;
            set {
                if (value >= 0 && value < _statesTransitions.Count)
                    _currentState = value;
                else
                    throw new IndexOutOfRangeException("The current state must be an existing state.");
            }
        }

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="initialState">Initial state of the automata.</param>
        /// <param name="finalStates">Final states of the automata.</param>
        /// <param name="statesTransitions">States transitions of the automata.</param>
        public OptimalFiniteAutomata(int initialState, ICollection<int> finalStates, ICollection<IDictionary<TSymbol, int>> statesTransitions)
        {
            if (finalStates == null)
                finalStates = new int[] { };

            if (statesTransitions == null)
                statesTransitions = new IDictionary<TSymbol, int>[] { };

            if (initialState < 0 || initialState >= statesTransitions.Count)
                throw new ArgumentException($"The initial state must be greather or equals 0 and less than { statesTransitions.Count }");

            this.InitialState = this._currentState = initialState;
            this._finalStates = new SortedSet<int>();

            foreach (int state in finalStates)
                this._finalStates.Add(state);

            this._statesTransitions = new List<SortedDictionary<TSymbol, int>>(statesTransitions.Count);

            foreach (IDictionary<TSymbol, int> stateTransitions in statesTransitions) {
                SortedDictionary<TSymbol, int> stateDictionary = new SortedDictionary<TSymbol, int>();
                this._statesTransitions.Add(stateDictionary);
                foreach (KeyValuePair<TSymbol, int> stateTransition in stateTransitions)
                    stateDictionary.Add(stateTransition.Key, stateTransition.Value);
            }
        }

        /// <summary>
        /// True if the current state is a final state
        /// </summary>
        /// <returns>True if the current state is a final state</returns>
        public bool IsInFinalState()
        {
            return _finalStates.Contains(_currentState);
        }

        /// <summary>
        /// Tries to process the input as a deterministic automata.
        /// </summary>
        /// <param name="input">Symbol to read</param>
        /// <returns>True if the automata could process the input</returns>
        public bool TryProcess(TSymbol symbol)
        {
            SortedDictionary<TSymbol, int> stateTransitions = _statesTransitions[_currentState];

            int destinationState;

            if (stateTransitions.TryGetValue(symbol, out destinationState)) {

                if (destinationState < 0 || destinationState >= _statesTransitions.Count)
                    throw new IndexOutOfRangeException($"There is no state with index { destinationState }. Please check the transition from state \"{ _currentState }\" and symbol \"{ symbol.GetSymbol() }\" .");

                _currentState = destinationState;
                return true;
            } else 
                return false;
        }

        /// <summary>
        /// Tries to process an input
        /// </summary>
        /// <param name="symbols">Symbols to process</param>
        /// <returns>Result of the operation</returns>
        public bool TryProcess(ICollection<TSymbol> symbols)
        {
            foreach (TSymbol symbol in symbols)
                if (!this.TryProcess(symbol))
                    return false;
            
            return true;
        }

    }
}

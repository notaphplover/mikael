﻿using Mikael.datastruct;
using Mikael.grammar.symbol;

using System.Collections.Generic;

namespace Mikael.automata.finite
{
    public interface IFiniteAutomata<TAutomata, TSymbol, TDictionary>
        where TAutomata : IFiniteAutomata<TAutomata, TSymbol, TDictionary>, new()
        where TSymbol : ISymbol<TSymbol>
        where TDictionary : IDictionary<TSymbol, MikaelSet<int>>, new()
    {
        #region PROPERTIES

        /// <summary>
        /// Current state of the automata
        /// </summary>
        int? CurrentState { get; set; }

        /// <summary>
        /// Final states of the automata
        /// </summary>
        ICollection<int> FinalStates { get; }

        /// <summary>
        /// Initial state of the automata.
        /// </summary>
        int? InitialState { get; }

        #endregion

        /// <summary>
        /// Creates a new state
        /// </summary>
        /// <returns>Index of the new state</returns>
        int AddState();

        /// <summary>
        /// Creates a new state
        /// </summary>
        /// <param name="transitions">Transitions of the state</param>
        /// <returns>Index of the new state</returns>
        int AddState(ICollection<KeyValuePair<TSymbol, ICollection<int>>> transitions);

        /// <summary>
        /// Generates the optimal automata from this one
        /// </summary>
        /// <returns>Optimal automata</returns>
        OptimalFiniteAutomata<TSymbol> GenerateOptimalAutomata();

        /// <summary>
        /// Obtains the state in an specific index
        /// </summary>
        /// <param name="index">Index of the state</param>
        /// <returns>State in the specific index</returns>
        TDictionary GetState(int index);

        /// <summary>
        /// Obtains the number of states in the automata
        /// </summary>
        /// <returns>Number of states in the automata</returns>
        int GetStates();

        /// <summary>
        /// Initializes this instance
        /// </summary>
        /// <param name="initialState">Initial state</param>
        /// <param name="finalStates"> Final states</param>
        /// <param name="lambdaSymbol">Lambda symbol</param>
        void Initialize(int? initialState, ICollection<int> finalStates, TSymbol lambdaSymbol);

        /// <summary>
        /// True if the current state is a final state
        /// </summary>
        /// <returns>True if the current state is a final state</returns>
        bool IsInFinalState();

        /// <summary>
        /// Obtains an automata that is the result of the minimization process over this automata
        /// </summary>
        /// <returns>Automata that is the result of the minimization process over this automata</returns>
        TAutomata MinimizedAutomata();

        /// <summary>
        /// Creates a new deterministic automata equivalent to this one.
        /// </summary>
        /// <returns>New deterministic automata equivalent to this one</returns>
        TAutomata ToDeterministicAutomata();

        /// <summary>
        /// Creates a new deterministic automata equivalent to this one.
        /// </summary>
        /// <param name="finalStatesTrace">Trace from the process. The keys are current final states. The values are old final states</param>
        /// <returns>New deterministic automata equivalent to this one</returns>
        TAutomata ToDeterministicAutomata(out SortedList<int, ICollection<int>> finalStatesTrace);

        /// <summary>
        /// Tries to process the input as a deterministic automata.
        /// </summary>
        /// <param name="input">Symbol to read</param>
        /// <returns>True if the automata could process the input</returns>
        bool TryDeterministicProcess(TSymbol symbol);

        /// <summary>
        /// Obtains an automata based on this automata. 
        /// All the states that can not access the final state or can not be accessed from the initial state will be removed
        /// </summary>
        /// <returns>Automata without dead states</returns>
        TAutomata WithoutDeadStates();
    }
}

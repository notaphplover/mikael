﻿using Mikael.datastruct;
using Mikael.grammar.symbol;

using System;
using System.Collections.Generic;

namespace Mikael.automata.finite
{
    /// <summary>
    /// Represents a finite automata
    /// </summary>
    /// <typeparam name="TAutomata">Type of the automata</typeparam>
    /// <typeparam name="TSymbol">Type of the symbols to be processed by the automata</typeparam>
    /// <typeparam name="TDictionary">Type of the dictionary used to contain state transitions</typeparam>
    public class FiniteAutomata<TAutomata, TSymbol, TDictionary> : IFiniteAutomata<TAutomata, TSymbol, TDictionary>
        where TAutomata : FiniteAutomata<TAutomata, TSymbol, TDictionary>, new()
        where TSymbol : ISymbol<TSymbol>
        where TDictionary : IDictionary<TSymbol, MikaelSet<int>>, new()
    {
        #region ATTRIBUTES

        /// <summary>
        /// Current state of the automata
        /// </summary>
        protected int? _currentState;

        /// <summary>
        /// Final states of the automata
        /// </summary>
        protected SortedSet<int> _finalStates;

        /// <summary>
        /// Initial state of the automata.
        /// </summary>
        protected int? _initialState;

        /// <summary>
        /// Lambda Symbol
        /// </summary>
        protected TSymbol _lambdaSymbol;

        /// <summary>
        /// Collection of states
        /// </summary>
        protected List<TDictionary> _states;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Current state of the automata
        /// </summary>
        public int? CurrentState {
            get => _currentState;
            set {
                if (value >= 0 && value < _states.Count)
                    _currentState = value;
                else
                    throw new IndexOutOfRangeException("The current state must be an existing state.");
            }
        }

        /// <summary>
        /// Final states of the automata
        /// </summary>
        public ICollection<int> FinalStates => _finalStates;

        /// <summary>
        /// Initial state of the automata.
        /// </summary>
        public int? InitialState => _initialState;

        #endregion

        /// <summary>
        /// Creates an empty Finite automata
        /// </summary>
        public FiniteAutomata()
        { }

        /// <summary>
        /// Creates a new Finite automata
        /// </summary>
        /// <param name="initialState">Initial state</param>
        /// <param name="finalStates"> Final states</param>
        /// <param name="lambdaSymbol">Lambda symbol</param>
        public FiniteAutomata(int? initialState, ICollection<int> finalStates, TSymbol lambdaSymbol)
        {
            this.Initialize(initialState, finalStates, lambdaSymbol);
        }

        /// <summary>
        /// Creates a new state
        /// </summary>
        /// <returns>Index of the new state</returns>
        public int AddState()
        {
            return this.AddState(null);
        }

        /// <summary>
        /// Creates a new state
        /// </summary>
        /// <param name="transitions">Transitions of the state</param>
        /// <returns>Index of the new state</returns>
        public int AddState(ICollection<KeyValuePair<TSymbol, ICollection<int>>> transitions)
        {
            TDictionary sortedList = new TDictionary();
            if (transitions != null)
                foreach (KeyValuePair<TSymbol, ICollection<int>> transition in transitions) {
                    MikaelSet<int> destinationStates = new MikaelSet<int>();

                    if (transition.Value != null)
                        foreach (int destinationState in transition.Value)
                            destinationStates.Add(destinationState);

                    sortedList.Add(transition.Key, destinationStates);
                }
                    
           
            int stateIndex = _states.Count;
            _states.Add(sortedList);
            return stateIndex;
        }

        /// <summary>
        /// Generates the optimal automata from this one
        /// </summary>
        /// <returns>Optimal automata</returns>
        public OptimalFiniteAutomata<TSymbol> GenerateOptimalAutomata()
        {
            if (!this._initialState.HasValue)
                throw new InvalidOperationException("the automata requires an initial state to perform this operation.");

            /*
             * The order is important. 
             * First of all, generate a deterministic automata. Minimization algorithm requires a deterministic automata 
             * and there is no point at calling WithoutDeadStates before because ToDeterministicAutomata method can 
             * introduce new dead states.
             * 
             * Then, we call MinimizedAutomata because it´s easy that the result of MinimizedAutomata call has a reduced
             * execution time over the call of WithoutDeadStates. It´s not so easy if we alter the order.
             */
            TAutomata automata = 
                this
                    .ToDeterministicAutomata()
                    .MinimizedAutomata()
                    .WithoutDeadStates()
            ;

            List<IDictionary<TSymbol, int>> newTransitions = new List<IDictionary<TSymbol, int>>(this._states.Count);

            for (int i = 0; i < automata._states.Count; ++i) {
                SortedDictionary<TSymbol, int> stateTransitions = new SortedDictionary<TSymbol, int>();
                newTransitions.Add(stateTransitions);

                foreach (KeyValuePair<TSymbol, MikaelSet<int>> currentStateTransitions in automata._states[i])
                    stateTransitions.Add(currentStateTransitions.Key, currentStateTransitions.Value.Min);
            }

            return new OptimalFiniteAutomata<TSymbol>(automata.InitialState.Value, automata.FinalStates, newTransitions);
        }

        /// <summary>
        /// Obtains the state in an specific index
        /// </summary>
        /// <param name="index">Index of the state</param>
        /// <returns>State in the specific index</returns>
        public TDictionary GetState(int index)
        {
            return _states[index];
        }

        /// <summary>
        /// Obtains the number of states in the automata
        /// </summary>
        /// <returns>Number of states in the automata</returns>
        public int GetStates()
        {
            return _states.Count;
        }

        /// <summary>
        /// Initializes this instance
        /// </summary>
        /// <param name="initialState">Initial state</param>
        /// <param name="finalStates"> Final states</param>
        /// <param name="lambdaSymbol">Lambda symbol</param>
        public virtual void Initialize(int? initialState, ICollection<int> finalStates, TSymbol lambdaSymbol)
        {
            if (!lambdaSymbol.GetSymbol().Equals(Symbol.LAMBDA_ALIAS))
                throw new ArgumentException($"lambdaSymbol must have the content of the constant Symbol.LAMBDA_ALIAS(\"{ Symbol.LAMBDA_ALIAS }\").");

            _finalStates = new SortedSet<int>();

            if (finalStates != null)
                foreach (int state in finalStates)
                    _finalStates.Add(state);

            _currentState = _initialState = initialState;
            _lambdaSymbol = lambdaSymbol;
            _states = new List<TDictionary>();
        }

        /// <summary>
        /// True if the current state is a final state
        /// </summary>
        /// <returns>True if the current state is a final state</returns>
        public bool IsInFinalState()
        {
            if (_currentState.HasValue)
                return _finalStates.Contains(_currentState.Value);
            else
                return false;
        }

        /// <summary>
        /// Tries to process the input as a deterministic automata.
        /// </summary>
        /// <param name="input">Symbol to read</param>
        /// <returns>True if the automata could process the input</returns>
        public bool TryDeterministicProcess(TSymbol symbol)
        {
            if (!_currentState.HasValue)
                throw new InvalidOperationException("The automata is not in a state.");

            if (_currentState < 0 || _currentState >= _states.Count)
                throw new IndexOutOfRangeException($"There is no state with index { _currentState }. Please check the transitions of this automata.");

            TDictionary stateTransitions = _states[_currentState.Value];

            MikaelSet<int> destinationStates;

            if (stateTransitions.TryGetValue(symbol, out destinationStates)) {
                if (destinationStates.Count == 1) {
                    _currentState = destinationStates.Min;
                    return true;
                } else {
                    return false;
                }
            } else 
                return false;
        }

        #region TO_DETERMINISTIC

        /// <summary>
        /// Creates a new deterministic automata equivalent to this one.
        /// </summary>
        /// <returns>New deterministic automata equivalent to this one</returns>
        public TAutomata ToDeterministicAutomata()
        {
            return this.ToDeterministicAutomata(out SortedList<int, ICollection<int>> finalStatesTrace);
        }

        /// <summary>
        /// Creates a new deterministic automata equivalent to this one.
        /// </summary>
        /// <param name="finalStatesTrace">Trace from the process. The keys are current final states. The values are old final states</param>
        /// <returns>New deterministic automata equivalent to this one</returns>
        public TAutomata ToDeterministicAutomata(out SortedList<int, ICollection<int>> finalStatesTrace)
        {
            //1. Copy all the states
            List<TDictionary> states = this.CloneStates(_states);

            SortedSet<int> newFinalStates;

            //2. Remove lambda rules
            this.ReduceLambdaRules(states, this._finalStates, out newFinalStates);

            //3. Generate sets of states.
            SortedDictionary<int, MikaelSet<int>> statesGroups = new SortedDictionary<int, MikaelSet<int>>();
            SortedDictionary<MikaelSet<int>, int> statesGroupsInverse = new SortedDictionary<MikaelSet<int>, int>();

            //3.1 Generate initial groups
            for (int i = 0; i < states.Count; ++i) {
                MikaelSet<int> group = new MikaelSet<int> { i };
                statesGroups.Add(i, group);
                statesGroupsInverse.Add(group, i);
            }

            //3.2 Generate complex groups
            this.GenerateComplexGroups(states, statesGroups, statesGroupsInverse);

            //4. Generate the new automata
            finalStatesTrace = new SortedList<int, ICollection<int>>();

            for (int i = _states.Count; i < statesGroups.Count; ++i) {
                statesGroups[i].IntersectWith(this._finalStates);
                if (statesGroups[i].Count > 0) {
                    finalStatesTrace.Add(i, new MikaelSet<int>(statesGroups[i]));
                    newFinalStates.Add(i);
                }
            }

            TAutomata deterministicAutomata = new TAutomata();
            deterministicAutomata.Initialize(this._initialState, newFinalStates, this._lambdaSymbol);
            deterministicAutomata._states = states;

            return deterministicAutomata;
        }

        /// <summary>
        /// Generates complex groups and states in order to remove complex transitions
        /// </summary>
        /// <param name="states">States set</param>
        /// <param name="statesGroups">Index to group dictionary</param>
        /// <param name="statesGroupsInverse">Group to index dictionary</param>
        private void GenerateComplexGroups(
            List<TDictionary> states, 
            SortedDictionary<int, MikaelSet<int>> statesGroups,
            SortedDictionary<MikaelSet<int>, int> statesGroupsInverse
        )
        {
            /*
             * Generate complex groups
             *
             * For each complex transition we will generate a new state as the join of all the destination states.
             * 
             * We will have finished once there are no complex transitions
             */
            int statesOffSet = 0;
            do {
                int statesNow = states.Count;
                for (int i = statesOffSet; i < statesNow; ++i) {
                    foreach (KeyValuePair<TSymbol, MikaelSet<int>> transition in states[i]) {
                        if (transition.Value.Count > 0) {
                            //We found a complex transition. Let´s create a new state.
                            MikaelSet<int> newGroupStates = new MikaelSet<int>();
                            foreach(int group in transition.Value) {
                                foreach (int state in statesGroups[group]) {
                                    newGroupStates.Add(state);
                                }
                            }
                            //Add the group if it does not exist
                            int groupIndex;
                            if (!statesGroupsInverse.TryGetValue(newGroupStates, out groupIndex)) {
                                groupIndex = statesGroups.Count;
                                statesGroups.Add(groupIndex, newGroupStates);
                                statesGroupsInverse.Add(newGroupStates, groupIndex);
                                //TODO: Generate a new state
                                TDictionary newState = new TDictionary();
                                foreach (int state in newGroupStates) {
                                    foreach (KeyValuePair<TSymbol, MikaelSet<int>> stateTransition in states[state]) {
                                        MikaelSet<int> newStateTransition;

                                        if (!newState.TryGetValue(stateTransition.Key, out newStateTransition)) {
                                            newStateTransition = new MikaelSet<int>();
                                            newState.Add(stateTransition.Key, newStateTransition);
                                        }

                                        foreach (int transitionDestination in stateTransition.Value)
                                            newStateTransition.Add(transitionDestination);
                                    }
                                }
                                states.Add(newState);
                            }
                            //Replace the transition
                            transition.Value.Clear();
                            transition.Value.Add(groupIndex);
                        }
                    }
                }
                statesOffSet = statesNow;
            } while (states.Count > statesOffSet);
        }

        #endregion

        #region REDUCE_LAMBDA

        /// <summary>
        /// Replaces all the lambda rules of the automata
        /// </summary>
        /// <param name="states">States of the automata</param>
        protected void ReduceLambdaRules(List<TDictionary> states, SortedSet<int> finalStates, out SortedSet<int> newFinalStates)
        {
            newFinalStates = new SortedSet<int>(finalStates);

            //1. Get Lambda jumps
            MikaelSet<int>[] lambdaJumps = this.GetLambdaClosure(states);

            //2. Remove all the lambda transitions.
            for (int i = 0; i < states.Count; ++i) {
                foreach (int destinationState in lambdaJumps[i]) 
                    if (finalStates.Contains(destinationState)) 
                        newFinalStates.Add(i);

                states[i].Remove(_lambdaSymbol);
            }
                
            /*
             * 3. We´ve got a group of states for each state. For each group, add transitions to the 
             * associated state s. For each transition t, add a transition from s to the
             * destination state of t.
             */
            this.AddEquivalentTransitions(states, lambdaJumps);
        }

        /// <summary>
        /// Obtains all the lambda closure of the states
        /// </summary>
        /// <param name="states">States of the finite automata</param>
        /// <returns>Collections of states that can be accesed from each state using only lambda transitions</returns>
        private MikaelSet<int>[] GetLambdaClosure(List<TDictionary> states)
        {
            MikaelSet<int>[] lambdaJumps = new MikaelSet<int>[states.Count];

            for (int i = 0; i < states.Count; ++i) {
                //Here we will save the states once they are processed
                lambdaJumps[i] = new MikaelSet<int>();
                Queue<int> statesToProcess = new Queue<int>();
                //Enqueue the state.
                statesToProcess.Enqueue(i);

                while (statesToProcess.Count > 0) {
                    int currentState = statesToProcess.Dequeue();

                    if (!lambdaJumps[i].Contains(currentState)) {
                        TDictionary state = states[currentState];

                        MikaelSet<int> lambdaTransitions;
                        if (state.TryGetValue(this._lambdaSymbol, out lambdaTransitions)) {
                            foreach (int destinationState in lambdaTransitions)
                                statesToProcess.Enqueue(destinationState);
                        }
                        //Now we´ve got all the jumps, we can add it as a processed state
                        lambdaJumps[i].Add(currentState);
                    }
                }
            }

            return lambdaJumps;
        }

        /// <summary>
        /// Adds transitions equivalent to removed lambda transitions
        /// </summary>
        /// <param name="states">states</param>
        /// <param name="lambdaJumps">Lambda jumps of the states</param>
        private void AddEquivalentTransitions(List<TDictionary> states, MikaelSet<int>[] lambdaJumps)
        {
            for (int i = 0; i < states.Count; ++i)
                foreach (int destinationState in lambdaJumps[i]) 
                    if (destinationState != i) 
                        foreach (KeyValuePair<TSymbol, MikaelSet<int>> nonLambdaTransitions in states[destinationState]) {
                            MikaelSet<int> destinationStates;
                            if (!states[i].TryGetValue(nonLambdaTransitions.Key, out destinationStates)) {
                                destinationStates = new MikaelSet<int>();
                                states[i].Add(nonLambdaTransitions.Key, destinationStates);
                            }
                            foreach (int transitionDestination in nonLambdaTransitions.Value)
                                destinationStates.Add(transitionDestination);
                        }
        }

        #endregion

        #region OPTIMIZATION

        #region PUBLIC

        /// <summary>
        /// Obtains an automata that is the result of the minimization process over this automata
        /// </summary>
        /// <returns>Automata that is the result of the minimization process over this automata</returns>
        public TAutomata MinimizedAutomata()
        {
            if (!_initialState.HasValue)
                throw new InvalidOperationException("An initial state is required");

            if (_finalStates == null || _finalStates.Count == 0)
                throw new InvalidOperationException("It is required to have at least one final state");

            /* 
             * 1. Create a group for each combination of transitions to final states 
             * concatenated with transitions to non final states from a state.
             */
            SortedDictionary<MinimizationVector<TSymbol>, MikaelSet<int>> statesGroups = this.CreateStatesGroups(this._states, this._finalStates);

            /*
             * We can ensure the following statements:
             * 1. The automata is deterministic.
             * 2. All the states of a group have set of transitions based on the same set of symbols
             */

            //Now, recalculate groups until no more groups are created
            this.ExpandStatesGroup(ref statesGroups);

            //Now it's time to generate a new automata with the delegates of each group.
            return this.CreateNewAutomata(statesGroups, this._initialState.Value, this._finalStates);
        }

        /// <summary>
        /// Obtains an automata based on this automata. 
        /// All the states that can not access the final state or can not be accessed from the initial state will be removed
        /// </summary>
        /// <returns>Automata without dead states</returns>
        public TAutomata WithoutDeadStates()
        {
            if (!_initialState.HasValue)
                throw new InvalidOperationException("An initial state is required");

            if (_finalStates == null || _finalStates.Count == 0)
                throw new InvalidOperationException("It is required to have at least one final state");

            //1. Build the direct jumps
            SortedList<int, MikaelSet<int>> states = this.CreateStatesGraph(_states);

            //2. Expand graph
            this.ExpandStatesGrapth(ref states);

            //Get the final states accesed by the initial state.
            MikaelSet<int> initialStateRange = states[_initialState.Value];

            //Get the final states that can be accesed from the initial state
            MikaelSet<int> finalStates; LinkedList<int> nonFinalStates;

            //Determine the new states of the automata
            this.ProcessStates(states, initialStateRange, out finalStates, out nonFinalStates);

            //Now we´ve got the states of the new automata. Let´s remove rules from or to states not contained in this set of new states.
            //Create a tree with the translations of the states.
            SortedList<int, int> statesTranslation = this.CreateStatesTranslation(states.Count, finalStates, nonFinalStates);

            List<TDictionary> newStates = new List<TDictionary>();

            TAutomata newAutomata = new TAutomata();

            //Set the final states
            SortedSet<int> newFinalStates = new MikaelSet<int>();

            foreach (int state in _finalStates)
                if (statesTranslation.TryGetValue(state, out int newState))
                    newFinalStates.Add(newState);

            newAutomata.Initialize(statesTranslation[_initialState.Value], newFinalStates, this._lambdaSymbol);

            //With the traslation tree we can create the new states.
            newAutomata._states = this.CreateNewAutomataStates(_states, statesTranslation);

            return newAutomata;
        }

        #endregion

        /// <summary>
        /// Creates a new automata based on the groups created using the minimization algorithm
        /// </summary>
        /// <param name="statesGroups">Groups created using the minimization algorithm</param>
        /// <param name="initialState">Initial state of the automata</param>
        /// <param name="finalStates">Final states of the automata</param>
        /// <returns>Automata created</returns>
        private TAutomata CreateNewAutomata(SortedDictionary<MinimizationVector<TSymbol>, MikaelSet<int>> statesGroups, int initialState, SortedSet<int> finalStates)
        {
            SortedDictionary<int, int> stateToGroupTranslations = this.CreateStatesGroupTranslations(statesGroups);

            TAutomata newAutomata = new TAutomata();

            MikaelSet<int> newFinalStates = new MikaelSet<int>();
            foreach (int finalState in finalStates)
                newFinalStates.Add(stateToGroupTranslations[finalState]);

            newAutomata.Initialize(stateToGroupTranslations[initialState], newFinalStates, this._lambdaSymbol);

            newAutomata._states = new List<TDictionary>(statesGroups.Count);

            foreach (KeyValuePair<MinimizationVector<TSymbol>, MikaelSet<int>> group in statesGroups) {
                TDictionary stateTransitions = new TDictionary();
                newAutomata._states.Insert(group.Key.OriginalGroup, stateTransitions);
                foreach (ComparablePair<TSymbol, int> transition in group.Key) {
                    stateTransitions.Add(transition.First, new MikaelSet<int> { transition.Second });
                }
            }

            return newAutomata;
        }

        /// <summary>
        /// Creates a new set of states 
        /// </summary>
        /// <param name="states"></param>
        /// <param name="statesTranslation"></param>
        /// <returns></returns>
        private List<TDictionary> CreateNewAutomataStates(List<TDictionary> states, SortedList<int, int> statesTranslation)
        {
            List<TDictionary> newStates = new List<TDictionary>(statesTranslation.Count);

            foreach (KeyValuePair<int, int> traslation in statesTranslation) {
                TDictionary newState = new TDictionary();
                newStates.Insert(traslation.Value, newState);
                if (states[traslation.Key] != null) {
                    foreach (KeyValuePair<TSymbol, MikaelSet<int>> stateTransitions in states[traslation.Key]) {
                        foreach (int transitionDestination in stateTransitions.Value) {
                            if (statesTranslation.ContainsKey(transitionDestination)) {
                                MikaelSet<int> newTransitionDestinations;

                                if (!newState.TryGetValue(stateTransitions.Key, out newTransitionDestinations)) {
                                    newTransitionDestinations = new MikaelSet<int>();
                                    newState.Add(stateTransitions.Key, newTransitionDestinations);
                                }

                                newTransitionDestinations.Add(statesTranslation[transitionDestination]);
                            }
                        }
                    }
                }
            }

            return newStates;
        }

        /// <summary>
        /// Creates a graph of states. Each key is an state. Each value is a set of states that can be
        /// directly accessed by the state.
        /// </summary>
        /// <param name="states">Set of states of an automata</param>
        /// <returns>States graph</returns>
        private SortedList<int, MikaelSet<int>> CreateStatesGraph(List<TDictionary> states)
        {
            SortedList<int, MikaelSet<int>> statesGraph = new SortedList<int, MikaelSet<int>>();

            for (int i = 0; i < states.Count; ++i)
                statesGraph[i] = new MikaelSet<int> { i };

            for (int i = 0; i < states.Count; ++i) {
                TDictionary state = states[i];
                MikaelSet<int> graphDestinationStates = statesGraph[i];
                foreach (MikaelSet<int> destinationStates in state.Values) 
                    foreach (int destinationState in destinationStates) 
                        graphDestinationStates.Add(destinationState);
            }

            return statesGraph;
        }

        /// <summary>
        /// Creates the translations from a state into the group that contains the state
        /// </summary>
        /// <param name="statesGroups">Set of states groups</param>
        /// <returns>Dictionary with the translations from a state to the group that contains the state</returns>
        public SortedDictionary<int, int> CreateStatesGroupTranslations(SortedDictionary<MinimizationVector<TSymbol>, MikaelSet<int>> statesGroups)
        {
            SortedDictionary<int, MikaelSet<int>> groupToStatesSet = new SortedDictionary<int, MikaelSet<int>>();
            {
                int counter = 0;
                foreach (KeyValuePair<MinimizationVector<TSymbol>, MikaelSet<int>> statesGroup in statesGroups) {
                    groupToStatesSet.Add(counter, statesGroup.Value);
                    ++counter;
                }
            }

            SortedDictionary<int, int> stateToGroup = new SortedDictionary<int, int>();
            
            foreach (KeyValuePair<int, MikaelSet<int>> translation in groupToStatesSet)
                foreach (int state in translation.Value)
                    stateToGroup.Add(state, translation.Key);

            return stateToGroup;
        }

        /// <summary>
        /// Creates a set of groups based on the automata states.
        /// 
        /// Each group will contain states whose transitions will have the same set of symbols.
        /// For each group, for each symbol in the set of the group, every state has a transition with the symbol to an state and:
        ///     1. All the transitions has a destination state that is a final state
        ///     or
        ///     2. All the transitions has a destination state that is not a final state
        /// 
        /// </summary>
        /// <param name="originalStates"></param>
        /// <returns></returns>
        public SortedDictionary<MinimizationVector<TSymbol>, MikaelSet<int>> CreateStatesGroups(List<TDictionary> originalStates, SortedSet<int> originalFinalStates)
        {
            SortedDictionary<MinimizationVector<TSymbol>, MikaelSet<int>> statesGroups = new SortedDictionary<MinimizationVector<TSymbol>, MikaelSet<int>>();

            int nonFinalGroup = 0;
            int finalGroup = 1;

            for (int i = 0; i < originalStates.Count; ++i) {
                TDictionary stateTransitions = originalStates[i];
                MinimizationVector<TSymbol> symbols;
                if (originalFinalStates.Contains(i))
                    symbols = new MinimizationVector<TSymbol>(finalGroup);
                else
                    symbols = new MinimizationVector<TSymbol>(nonFinalGroup);

                foreach (KeyValuePair<TSymbol, MikaelSet<int>> symbolTransitions in stateTransitions) {
                    if (symbolTransitions.Value.Count != 1)
                        throw new InvalidOperationException("This operation requires a deterministic automata.");
                    else {
                        if (_finalStates.Contains(symbolTransitions.Value.Min)) {
                            symbols.Add(new ComparablePair<TSymbol, int>(symbolTransitions.Key, finalGroup));
                        } else
                            symbols.Add(new ComparablePair<TSymbol, int>(symbolTransitions.Key, nonFinalGroup));
                    }  
                }

                MikaelSet<int> states;

                if (!statesGroups.TryGetValue(symbols, out states)) {
                    states = new MikaelSet<int>();
                    statesGroups.Add(symbols, states);
                }

                states.Add(i);
            }

            return statesGroups;
        }

        /// <summary>
        /// Creates a dictionary to translate old states indexes into new states indexes
        /// </summary>
        /// <param name="states">Amount of states</param>
        /// <param name="finalStates">Set of new final states</param>
        /// <param name="nonFinalStates">Set of non final states</param>
        /// <returns>Dictionary to translate old states indexes into new states indexes</returns>
        private SortedList<int, int> CreateStatesTranslation(int states, MikaelSet<int> finalStates, LinkedList<int> nonFinalStates)
        {
            SortedList<int, int> statesTranslation = new SortedList<int, int>();

            int offSet = 0;
            for (int i = 0; i < states; ++i) {
                if (finalStates.Contains(i) || nonFinalStates.Contains(i)) {
                    statesTranslation.Add(i, i - offSet);
                } else
                    ++offSet;
            }

            return statesTranslation;
        }

        /// <summary>
        /// Expands a graph of states. The final graph will contain the states that can be accessed,
        /// directly or not, by the state.
        /// </summary>
        /// <param name="states">States graph</param>
        protected void ExpandStatesGrapth(ref SortedList<int, MikaelSet<int>> states)
        {
            bool stayInLoop = false;

            do {
                SortedList<int, MikaelSet<int>> newStates = new SortedList<int, MikaelSet<int>>();

                // Calculate states accesible from each state
                foreach (KeyValuePair<int, MikaelSet<int>> stateJumps in states) {
                    foreach (int destinationState in stateJumps.Value) {
                        MikaelSet<int> destinationStateTransitions;

                        if (states.TryGetValue(destinationState, out destinationStateTransitions)) {
                            foreach (int destinationStateTransitionDestination in destinationStateTransitions) {
                                //Add the states to the new state
                                MikaelSet<int> newDestinationStates;

                                if (!newStates.TryGetValue(stateJumps.Key, out newDestinationStates)) {
                                    newDestinationStates = new MikaelSet<int>();
                                    newStates.Add(stateJumps.Key, newDestinationStates);
                                }

                                newDestinationStates.Add(destinationStateTransitionDestination);
                            }
                        }
                    }

                    if (!Object.Equals(newStates[stateJumps.Key], states[stateJumps.Key]))
                        stayInLoop = true;
                }

                states = newStates;
            } while (!stayInLoop);
        }

        /// <summary>
        /// Expands the states groups until no more groups can be created
        /// </summary>
        /// <param name="statesGroups">States groups to be expanded</param>
        protected void ExpandStatesGroup(ref SortedDictionary<MinimizationVector<TSymbol>, MikaelSet<int>> statesGroups)
        {
            SortedDictionary<MinimizationVector<TSymbol>, MikaelSet<int>> newStatesGroup;
            bool repeatLoop;
            do {
                // Create the translations
                SortedDictionary<int, int> stateToGroup = this.CreateStatesGroupTranslations(statesGroups);

                //Create a new partition of states
                newStatesGroup = new SortedDictionary<MinimizationVector<TSymbol>, MikaelSet<int>>();

                foreach (KeyValuePair<MinimizationVector<TSymbol>, MikaelSet<int>> statesGroup in statesGroups) {
                    foreach (int state in statesGroup.Value) {
                        MinimizationVector<TSymbol> transitionGroups = new MinimizationVector<TSymbol>(stateToGroup[state]);
                        foreach (ComparablePair<TSymbol, int> transitionToGroup in statesGroup.Key) {
                            //Create the new group
                            int destinationState = this._states[state][transitionToGroup.First].Min;
                            //Get the group
                            int group = stateToGroup[destinationState];
                            transitionGroups.Add(new ComparablePair<TSymbol, int>(transitionToGroup.First, group));
                        }

                        MikaelSet<int> groupStates;

                        if (!newStatesGroup.TryGetValue(transitionGroups, out groupStates)) {
                            groupStates = new MikaelSet<int>();
                            newStatesGroup.Add(transitionGroups, groupStates);
                        }

                        groupStates.Add(state);
                    }
                }

                repeatLoop = statesGroups.Count != newStatesGroup.Count;
                statesGroups = newStatesGroup;
            } while (repeatLoop);
        }

        /// <summary>
        /// Determines the states that must remain as non final states and the states that must remain as final states
        /// </summary>
        /// <param name="states">States graph</param>
        /// <param name="initialStateRange">States that can be accessed, directly or not, by the initial state</param>
        /// <param name="finalStates">New set of final states</param>
        /// <param name="nonFinalStates">New set of non final states</param>
        private void ProcessStates(SortedList<int, MikaelSet<int>> states, MikaelSet<int> initialStateRange, out MikaelSet<int> finalStates, out LinkedList<int> nonFinalStates)
        {
            finalStates = new MikaelSet<int>();
            nonFinalStates = new LinkedList<int>();
            foreach (int state in initialStateRange) {
                if (_finalStates.Contains(state))
                    finalStates.Add(state);
                else
                    nonFinalStates.AddLast(state);
            }

            //Add the initial state
            if (_finalStates.Contains(_initialState.Value))
                finalStates.Add(_initialState.Value);
            else
                nonFinalStates.AddFirst(_initialState.Value);

            /*
             * Determine, for each non final state if it can jump to a final state from the new set. 
             * Those that can not do that must be removed.
             */
            {
                LinkedListNode<int> nonFinalStatesPivot = nonFinalStates.First;
                while (nonFinalStatesPivot != null) {
                    MikaelSet<int> statesAccesedByNonFinalState;
                    if (states.TryGetValue(nonFinalStatesPivot.Value, out statesAccesedByNonFinalState)) {
                        if (statesAccesedByNonFinalState.Overlaps(finalStates)) {
                            nonFinalStatesPivot = nonFinalStatesPivot.Next;
                        } else {
                            //Remove the state
                            nonFinalStates.Remove(nonFinalStatesPivot);
                            nonFinalStatesPivot = nonFinalStatesPivot.Next;
                        }
                    } else {
                        //Remove the state
                        nonFinalStates.Remove(nonFinalStatesPivot);
                        nonFinalStatesPivot = nonFinalStatesPivot.Next;
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// Clones a set of states
        /// </summary>
        /// <param name="states">Set of states</param>
        /// <returns>Clone of the states set</returns>
        protected List<TDictionary> CloneStates(List<TDictionary> states)
        {
            List<TDictionary> newStates = new List<TDictionary>();

            //1. Copy all the states
            foreach (TDictionary state in states) {
                TDictionary newStateTransitions = new TDictionary();
                
                foreach (KeyValuePair<TSymbol, MikaelSet<int>> stateTransition in state) {
                    MikaelSet<int> newDestinationStates = new MikaelSet<int>();
                    foreach (int destinationState in stateTransition.Value)
                        newDestinationStates.Add(destinationState);

                    newStateTransitions.Add(stateTransition.Key, newDestinationStates);
                }
                newStates.Add(newStateTransitions);
            }

            return newStates;
        }
    }

    /// <summary>
    /// Represents a finite automata
    /// </summary>
    /// <typeparam name="TSymbol">Type of the symbols to be processed by the automata</typeparam>
    public class FiniteAutomata<TSymbol> : FiniteAutomata<FiniteAutomata<TSymbol>, TSymbol, SortedDictionary<TSymbol, MikaelSet<int>>>
        where TSymbol : ISymbol<TSymbol>
    {
        /// <summary>
        /// Creates a new Finite automata
        /// </summary>
        public FiniteAutomata()
            : base()
        { }

        /// <summary>
        /// Creates a new Finite automata
        /// </summary>
        /// <param name="initialState">Initial state</param>
        /// <param name="finalStates"> Final states</param>
        /// <param name="lambdaSymbol">Lambda symbol</param>
        public FiniteAutomata(int initialState, ICollection<int> finalStates, TSymbol lambdaSymbol)
            : base(initialState, finalStates, lambdaSymbol)
        { }
    }

    public class MinimizationVector<TSymbol> : MikaelSet<ComparablePair<TSymbol, int>, MinimizationVector<TSymbol>>
        where TSymbol : ISymbol<TSymbol>
    {
        #region ATTRIBUTES 

        /// <summary>
        /// Original group of the states associated
        /// </summary>
        protected int _originalGroup;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Original group of the states associated
        /// </summary>
        public int OriginalGroup {
            get { return _originalGroup; }
            set { _originalGroup = value; }
        }

        #endregion

        public MinimizationVector()
            : base()
        { }

        public MinimizationVector(int originalGroup)
            : base()
        {
            _originalGroup = originalGroup;
        }

        public override int CompareTo(MinimizationVector<TSymbol> other)
        {
            int firstComparison = this._originalGroup.CompareTo(other._originalGroup);

            if (firstComparison == 0)
                return base.CompareTo(other);
            else
                return firstComparison;
        }
    }
}

﻿using Mikael.grammar.rule;
using Mikael.grammar.symbol;

using System.Collections.Generic;

namespace Mikael.grammar
{
    /// <summary>
    /// Represents a grammar
    /// </summary>
    /// <typeparam name="TRule">Type of the rules</typeparam>
    /// <typeparam name="TInput">Type of the input of the rules</typeparam>
    /// <typeparam name="TOutput">Type of the output of the rules</typeparam>
    /// <typeparam name="TSymbol">Type of the symbols</typeparam>
    public interface IGrammar<TRule, TInput, TOutput, TSymbol>
        where TRule : IRule<TRule, TInput, TOutput, TSymbol>, new()
        where TInput : ISymbols<TInput, TSymbol>
        where TOutput : ISymbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        #region ATTRIBUTES

        /// <summary>
        /// Initial symbol of the grammar
        /// </summary>
        TSymbol InitialSymbol { get; }

        /// <summary>
        /// Terminal symbols of the grammar
        /// </summary>
        SortedSet<TSymbol> TerminalSymbols { get; }

        /// <summary>
        /// Non terminal symbols of the grammar
        /// </summary>
        SortedSet<TSymbol> NonTerminalSymbols { get; }

        #endregion

        /// <summary>
        /// Obtains the rules of the grammar
        /// </summary>
        /// <returns>Rules of the grammar</returns>
        ICollection<TRule> GetRules();
    }

    /// <summary>
    /// Represents a context free grammar
    /// </summary>
    /// <typeparam name="TRule">Type of the rules</typeparam>
    /// <typeparam name="TOutput">Type of the output of the rules</typeparam>
    /// <typeparam name="TSymbol">Type of the symbols</typeparam>
    public interface IContextFreeGrammar<TRule, TOutput, TSymbol> : IGrammar<TRule, TSymbol, TOutput, TSymbol>
        where TRule : IRule<TRule, TSymbol, TOutput, TSymbol>, new()
        where TOutput : ISymbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    { }

    /// <summary>
    /// Represents a regular grammar
    /// </summary>
    /// <typeparam name="TRule">Type of the rules</typeparam>
    /// <typeparam name="TOutput">Type of the output of the rules</typeparam>
    /// <typeparam name="TSymbol">Type of the symbols</typeparam>
    public interface IRegularGrammar<TRule, TOutput, TSymbol> : IContextFreeGrammar<TRule, TOutput, TSymbol>
        where TRule : IG3Rule<TRule, TOutput, TSymbol>, new()
        where TOutput : IG3Symbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    { }
}
﻿using System;
using System.Collections.Generic;

namespace Mikael.grammar.symbol
{
    /// <summary>
    /// Represents a set of symbols
    /// </summary>
    public interface ISymbols
    {
        
    }

    /// <summary>
    /// Represents a set of symbols
    /// </summary>
    /// <typeparam name="T">Type of the comparable and equatable instances</typeparam>
    public interface ISymbols<T, TInnerSymbol> : ISymbols, IComparable<T>, IEquatable<T>
        where TInnerSymbol : ISymbol<TInnerSymbol>
        where T : ISymbols<T, TInnerSymbol>
    {
        /// <summary>
        /// Obtains the collection of symbols in this instance
        /// </summary>
        /// <returns>Collection of symbols in this instance</returns>
        ICollection<TInnerSymbol> GetSymbols();
    }
}

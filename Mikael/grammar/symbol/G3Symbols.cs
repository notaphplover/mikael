﻿namespace Mikael.grammar.symbol
{
    /// <summary>
    /// Represents a G3 left generarion
    /// </summary>
    /// <typeparam name="T">Type of the symbols</typeparam>
    public class G3LeftSymbols<TInnerSymbol> : Symbols<G3LeftSymbols<TInnerSymbol>, TInnerSymbol>, IG3LeftSymbols<G3LeftSymbols<TInnerSymbol>, TInnerSymbol>
        where TInnerSymbol : ISymbol<TInnerSymbol>
    {
        /// <summary>
        /// Index of the non terminal element in the symbols string
        /// </summary>
        protected const int NT_INDEX = 0;

        /// <summary>
        /// Index of the terminal element in the symbols string
        /// </summary>
        protected const int T_INDEX = 1;

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="ntSymbol">Non terminal symbol</param>
        /// <param name="tSymbol">Terminal symbol</param>
        public G3LeftSymbols(TInnerSymbol ntSymbol, TInnerSymbol tSymbol)
            : base(new TInnerSymbol[] { ntSymbol, tSymbol })
        { }

        /// <summary>
        /// Non terminal symbol
        /// </summary>
        public TInnerSymbol NTSymbol => this._symbols[NT_INDEX];

        /// <summary>
        /// Terminal symbol
        /// </summary>
        public TInnerSymbol TSymbol => this._symbols[T_INDEX];
    }

    /// <summary>
    /// Represents a G3 left generarion
    /// </summary>
    /// <typeparam name="T">Type of the symbols</typeparam>
    public class G3RightSymbols<TInnerSymbol> : Symbols<G3RightSymbols<TInnerSymbol>, TInnerSymbol>, IG3RightSymbols<G3RightSymbols<TInnerSymbol>, TInnerSymbol>
        where TInnerSymbol : ISymbol<TInnerSymbol>
    {
        /// <summary>
        /// Index of the non terminal element in the symbols string
        /// </summary>
        protected const int NT_INDEX = 1;

        /// <summary>
        /// Index of the terminal element in the symbols string
        /// </summary>
        protected const int T_INDEX = 0;

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="ntSymbol">Non terminal symbol</param>
        /// <param name="tSymbol">Terminal symbol</param>
        public G3RightSymbols(TInnerSymbol ntSymbol, TInnerSymbol tSymbol)
            : base(new TInnerSymbol[] { tSymbol, ntSymbol })
        { }

        /// <summary>
        /// Non terminal symbol
        /// </summary>
        public TInnerSymbol NTSymbol => this._symbols[NT_INDEX];

        /// <summary>
        /// Terminal symbol
        /// </summary>
        public TInnerSymbol TSymbol => this._symbols[T_INDEX];
    }
}

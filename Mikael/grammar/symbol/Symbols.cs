﻿using System.Collections.Generic;

namespace Mikael.grammar.symbol
{
    /// <summary>
    /// Represents a set of symbols
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Symbols<T, TInnerSymbol> : ISymbols<T, TInnerSymbol>
        where T : ISymbols<T, TInnerSymbol>
        where TInnerSymbol : ISymbol<TInnerSymbol>
    {
        /// <summary>
        /// Collection of symbols in this instance
        /// </summary>
        protected TInnerSymbol[] _symbols;

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="symbols">Symbols that will be contained in the instance</param>
        public Symbols(TInnerSymbol[] symbols)
        {
            _symbols = symbols;
        }

        /// <summary>
        /// Compares this instance to another one
        /// </summary>
        /// <param name="other">Instance to compare with this instance</param>
        /// <returns>Result of the operation</returns>
        public int CompareTo(T other)
        {
            IEnumerator<TInnerSymbol> mySymbols = this.GetSymbols().GetEnumerator();
            IEnumerator<TInnerSymbol> otherSymbols = other.GetSymbols().GetEnumerator();

            while (mySymbols.MoveNext()) {
                if (otherSymbols.MoveNext()) {
                    int comparison = mySymbols.Current.CompareTo(otherSymbols.Current);
                    if (comparison != 0)
                        return comparison;
                } else
                    return 1;
            }

            return otherSymbols.MoveNext() ? -1 : 0;
        }

        /// <summary>
        /// Determines if this instance is the same as another instance
        /// </summary>
        /// <param name="other">Instance to compare with this instance</param>
        /// <returns>Result of the operation</returns>
        public bool Equals(T other)
        {
            if (this.GetSymbols().Count != other.GetSymbols().Count)
                return false;

            IEnumerator<TInnerSymbol> mySymbols = this.GetSymbols().GetEnumerator();
            IEnumerator<TInnerSymbol> otherSymbols = other.GetSymbols().GetEnumerator();

            while (mySymbols.MoveNext()) {
                otherSymbols.MoveNext();
                if (!mySymbols.Current.Equals(otherSymbols.Current))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Obtains the collection of symbols in this instance
        /// </summary>
        /// <returns>Collection of symbols in this instance</returns>
        public ICollection<TInnerSymbol> GetSymbols()
        {
            return this._symbols;
        }
    }

    /// <summary>
    /// Represents a set of symbols
    /// </summary>
    public class Symbols<TInnerSymbol> : Symbols<Symbols<TInnerSymbol>, TInnerSymbol>
        where TInnerSymbol : ISymbol<TInnerSymbol>
    {
        public Symbols(TInnerSymbol[] symbols) 
            : base(symbols)
        { }
    }
}

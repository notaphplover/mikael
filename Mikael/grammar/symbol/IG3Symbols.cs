﻿namespace Mikael.grammar.symbol
{
    /// <summary>
    /// Represents a G3 generation.
    /// </summary>
    /// <typeparam name="T">Type of the symbols</typeparam>
    public interface IG3Symbols<T, TInnerSymbol> : ISymbols<T, TInnerSymbol>
        where T : IG3Symbols<T, TInnerSymbol>
        where TInnerSymbol : ISymbol<TInnerSymbol>
    {
        /// <summary>
        /// Non terminal symbol
        /// </summary>
        TInnerSymbol NTSymbol { get; }

        /// <summary>
        /// Terminal symbol
        /// </summary>
        TInnerSymbol TSymbol { get; }
    }

    /// <summary>
    /// Represents a G3 left generation
    /// </summary>
    /// <typeparam name="T">Type of the symbols</typeparam>
    public interface IG3LeftSymbols<T, TInnerSymbol> : IG3Symbols<T, TInnerSymbol>
        where TInnerSymbol : ISymbol<TInnerSymbol>
        where T : IG3LeftSymbols<T, TInnerSymbol>
    { }

    /// <summary>
    /// Represents a G3 right generation
    /// </summary>
    /// <typeparam name="T">Type of the symbols</typeparam>
    public interface IG3RightSymbols<T, TInnerSymbol> : IG3Symbols<T, TInnerSymbol>
        where TInnerSymbol : ISymbol<TInnerSymbol>
        where T : IG3RightSymbols<T, TInnerSymbol>
    { }
}

﻿using System.Collections.Generic;

namespace Mikael.grammar.symbol
{
    /// <summary>
    /// Represents a Symbol
    /// </summary>
    public class Symbol : ISymbol<Symbol>
    {
        #region CONST

        /// <summary>
        /// Alias of the lambda symbol. Any symbols with this value will be considered as the lambda symbol
        /// </summary>
        public const string LAMBDA_ALIAS = "__λ__";

        #endregion

        /// <summary>
        /// Value of the symbol
        /// </summary>
        protected string _value;

        /// <summary>
        /// Creates a new symbol
        /// </summary>
        /// <param name="value">Value of the symbol</param>
        public Symbol(string value)
        {
            _value = value;
        }

        /// <summary>
        /// Compares this instance to another one
        /// </summary>
        /// <param name="other">Instance to compare with this instance</param>
        /// <returns>Result of the operation</returns>
        public int CompareTo(Symbol other)
        {
            return _value.CompareTo(other._value);
        }

        /// <summary>
        /// Determines if this instance is the same as another instance
        /// </summary>
        /// <param name="other">Instance to compare with this instance</param>
        /// <returns>Result of the operation</returns>
        public bool Equals(Symbol other)
        {
            return _value.Equals(other._value);
        }

        /// <summary>
        /// Obtains the value of this symbol
        /// </summary>
        /// <returns></returns>
        public string GetSymbol()
        {
            return _value;
        }

        /// <summary>
        /// Obtains the collection of symbols in this instance (just one symbol)
        /// </summary>
        /// <returns>Collection of symbols in this instance</returns>
        public ICollection<Symbol> GetSymbols()
        {
            return new Symbol[] { this };
        }
    }
}

﻿namespace Mikael.grammar.symbol
{
    /// <summary>
    /// Represents a symbol
    /// </summary>
    public interface ISymbol : ISymbols
    {
        /// <summary>
        /// Obtains the symbol in this instance
        /// </summary>
        /// <returns>Symbol in this instance</returns>
        string GetSymbol();
    }

    /// <summary>
    /// Represents a symbol
    /// </summary>
    public interface ISymbol<T> : ISymbol, ISymbols<T, T>
        where T : ISymbol<T>
    { }
}

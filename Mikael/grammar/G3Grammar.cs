﻿using Mikael.grammar.rule;
using Mikael.grammar.symbol;

using System.Collections.Generic;

namespace Mikael.grammar
{
    /// <summary>
    /// Represents a G3 grammar
    /// </summary>
    /// <typeparam name="TRule">Type of the rules used in the grammar</typeparam>
    /// <typeparam name="TOutput">Type of the output of the rules used in the grammar</typeparam>
    /// <typeparam name="TSymbol">Type of the symbols used in the grammar</typeparam>
    public class G3Grammar<TRule, TOutput, TSymbol> : Grammar<TRule, TSymbol, TOutput, TSymbol>, IRegularGrammar<TRule, TOutput, TSymbol>
        where TRule : IG3Rule<TRule, TOutput, TSymbol>, new()
        where TOutput : IG3Symbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="initialSymbol">Initial symbol of the grammar.</param>
        /// <param name="rules">Set of rules of the grammar.</param>
        /// <param name="nonTerminalSymbols">Set of non terminal symbols of the grammar.</param>
        /// <param name="terminalSymbols">Set of terminal symbols of the grammar.</param>
        public G3Grammar(TSymbol initialSymbol, ICollection<TRule> rules, ICollection<TSymbol> nonTerminalSymbols, ICollection<TSymbol> terminalSymbols)
            : base(initialSymbol, rules, nonTerminalSymbols, terminalSymbols)
        { }
    }

    /// <summary>
    /// Represents a G3 Right grammar.
    /// </summary>
    /// <typeparam name="TSymbol">Type of the symbols used in the grammar</typeparam>
    public class G3RightGrammar<TSymbol> : G3Grammar<G3RightRule<TSymbol>, G3RightSymbols<TSymbol>, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="initialSymbol">Initial symbol of the grammar.</param>
        /// <param name="rules">Set of rules of the grammar.</param>
        /// <param name="nonTerminalSymbols">Set of non terminal symbols of the grammar.</param>
        /// <param name="terminalSymbols">Set of terminal symbols of the grammar.</param>
        public G3RightGrammar(TSymbol initialSymbol, ICollection<G3RightRule<TSymbol>> rules, ICollection<TSymbol> nonTerminalSymbols, ICollection<TSymbol> terminalSymbols)
            : base(initialSymbol, rules, nonTerminalSymbols, terminalSymbols)
        { }
    }

    /// <summary>
    /// Represents a G3 Left grammar.
    /// </summary>
    /// <typeparam name="TSymbol">Type of the symbols used in the grammar</typeparam>
    public class G3LeftGrammar<TSymbol> : G3Grammar<G3LeftRule<TSymbol>, G3LeftSymbols<TSymbol>, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="initialSymbol">Initial symbol of the grammar.</param>
        /// <param name="rules">Set of rules of the grammar.</param>
        /// <param name="nonTerminalSymbols">Set of non terminal symbols of the grammar.</param>
        /// <param name="terminalSymbols">Set of terminal symbols of the grammar.</param>
        public G3LeftGrammar(TSymbol initialSymbol, ICollection<G3LeftRule<TSymbol>> rules, ICollection<TSymbol> nonTerminalSymbols, ICollection<TSymbol> terminalSymbols)
            : base(initialSymbol, rules, nonTerminalSymbols, terminalSymbols)
        { }
    }

}

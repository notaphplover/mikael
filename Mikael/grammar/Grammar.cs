﻿using Mikael.grammar.rule;
using Mikael.grammar.symbol;

using System;
using System.Collections.Generic;

namespace Mikael.grammar
{
    public abstract class Grammar<TRule, TInput, TOutput, TSymbol> : IGrammar<TRule, TInput, TOutput, TSymbol>
        where TRule : IRule<TRule, TInput, TOutput, TSymbol>, new()
        where TInput : ISymbols<TInput, TSymbol>
        where TOutput : ISymbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        #region ATTRIBUTES

        /// <summary>
        /// Set of rules
        /// </summary>
        protected SortedDictionary<TInput, LinkedList<TOutput>> _rules;

        #endregion

        #region PROPERTIES

        /// <summary>
        /// Initial symbol of the grammar
        /// </summary>
        public TSymbol InitialSymbol { get; protected set; }

        /// <summary>
        /// Terminal symbols of the grammar
        /// </summary>
        public SortedSet<TSymbol> TerminalSymbols { get; protected set; }

        /// <summary>
        /// Non terminal symbols of the grammar
        /// </summary>
        public SortedSet<TSymbol> NonTerminalSymbols { get; protected set; }

        #endregion

        /// <summary>
        /// Creates a new instance of this class.
        /// </summary>
        /// <param name="initialSymbol">Initial symbol of the grammar.</param>
        /// <param name="rules">Set of rules of the grammar.</param>
        /// <param name="nonTerminalSymbols">Set of non terminal symbols of the grammar.</param>
        /// <param name="terminalSymbols">Set of terminal symbols of the grammar.</param>
        public Grammar(TSymbol initialSymbol, ICollection<TRule> rules, ICollection<TSymbol> nonTerminalSymbols, ICollection<TSymbol> terminalSymbols)
        {
            this.InitialSymbol = initialSymbol;
            this.TerminalSymbols = new SortedSet<TSymbol>();
            this.NonTerminalSymbols = new SortedSet<TSymbol>();
            this._rules = new SortedDictionary<TInput, LinkedList<TOutput>>();

            //Fill the trees

            foreach (TSymbol symbol in terminalSymbols)
                this.TerminalSymbols.Add(symbol);

            foreach (TSymbol symbol in nonTerminalSymbols)
                this.NonTerminalSymbols.Add(symbol);

            //Terminal symbols and non terminal symbols must be different
            if (this.TerminalSymbols.Overlaps(this.NonTerminalSymbols))
                throw new Exception("Terminal symbols and non terminal symbols must be different. Common symbols were found.");

            this.AddRules(rules);
        }

        /// <summary>
        /// Obtains the rules of the grammar
        /// </summary>
        /// <returns>Rules of the grammar</returns>
        public ICollection<TRule> GetRules()
        {
            LinkedList<TRule> rules = new LinkedList<TRule>();

            foreach (KeyValuePair<TInput, LinkedList<TOutput>> subRules in this._rules)
                foreach (TOutput generation in subRules.Value) {
                    TRule rule = new TRule();
                    rule.Initialize(subRules.Key, generation);
                    rules.AddLast(rule);
                }

            return rules;
        }

        /// <summary>
        /// Adds rules to the grammar
        /// </summary>
        /// <param name="rules">Se t of rules to be added</param>
        public virtual void AddRules(ICollection<TRule> rules)
        {
            this.CheckRules(rules);

            foreach (TRule rule in rules) {
                LinkedList<TOutput> list;

                if (!this._rules.TryGetValue(rule.Input, out list)) {
                    list = new LinkedList<TOutput>();
                    this._rules.Add(rule.Input, list);
                }

                list.AddLast(rule.Output);
            }
        }

        /// <summary>
        /// Ensures that a set of rules is correct in the current grammar´s context. 
        /// </summary>
        /// <param name="rules">Set of rules</param>
        protected virtual void CheckRules(ICollection<TRule> rules)
        {
            foreach (TRule rule in rules) {
                int nonTerminalInputSymbols = 0;
                foreach (TSymbol symbol in rule.Input.GetSymbols())
                    if (this.NonTerminalSymbols.Contains(symbol))
                        ++nonTerminalInputSymbols;
                    else if (!this.TerminalSymbols.Contains(symbol) && !Symbol.LAMBDA_ALIAS.Equals(symbol.GetSymbol()))
                        throw new ArgumentException("Only terminal, non terminal and lambda symbols are allowed");

                if (nonTerminalInputSymbols == 0)
                    throw new Exception("A terminal symbol can not generate any other symbol. Please check the rules added to the grammar.");
            }
                
        }
    }
}

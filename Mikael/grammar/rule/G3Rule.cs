﻿using Mikael.grammar.symbol;

namespace Mikael.grammar.rule
{
    /// <summary>
    /// Represents a G3 Rule
    /// </summary>
    public abstract class G3Rule<TRule, TOutput, TSymbol> : Rule<TRule, TSymbol, TOutput, TSymbol>, IG3Rule<TRule, TOutput, TSymbol>
        where TRule : G3Rule<TRule, TOutput, TSymbol>, new()
        where TOutput : IG3Symbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        public G3Rule() : base() { }

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="input">Input part of the rule</param>
        /// <param name="output">Output part of the rule</param>
        public G3Rule(TSymbol input, TOutput output) : base(input, output) { }
    }

    /// <summary>
    /// Represents a G3 Left Rule
    /// </summary>
    public class G3LeftRule<TSymbol> : G3Rule<G3LeftRule<TSymbol>, G3LeftSymbols<TSymbol>, TSymbol>, IG3LeftRule<G3LeftRule<TSymbol>, G3LeftSymbols<TSymbol>, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        public G3LeftRule() : base() { }

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="input">Input part of the rule</param>
        /// <param name="output">Output part of the rule</param>
        public G3LeftRule(TSymbol input, G3LeftSymbols<TSymbol> output) : base(input, output) { }
    }

    /// <summary>
    /// Represents a G3 Right Rule
    /// </summary>
    public class G3RightRule<TSymbol> : G3Rule<G3RightRule<TSymbol>, G3RightSymbols<TSymbol>, TSymbol>, IG3RightRule<G3RightRule<TSymbol>, G3RightSymbols<TSymbol>, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        public G3RightRule() : base() { }

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="input">Input part of the rule</param>
        /// <param name="output">Output part of the rule</param>
        public G3RightRule(TSymbol input, G3RightSymbols<TSymbol> output) : base(input, output) { }
    }
}

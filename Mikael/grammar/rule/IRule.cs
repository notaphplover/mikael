﻿using Mikael.grammar.symbol;

using System;

namespace Mikael.grammar.rule
{
    /// <summary>
    /// Represents a rule
    /// </summary>
    /// <typeparam name="TRule">Type of the rule</typeparam>
    /// <typeparam name="TInput">Input type</typeparam>
    /// <typeparam name="TOutput">Output type</typeparam>
    public interface IRule<TRule, TInput, TOutput, TSymbol> : IComparable<TRule>, IEquatable<TRule>
        where TRule : IRule<TRule, TInput, TOutput, TSymbol>, new()
        where TInput : ISymbols<TInput, TSymbol>
        where TOutput : ISymbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        #region PROPERTIES

        /// <summary>
        /// Input part of the rule
        /// </summary>
        TInput Input { get; set; }

        /// <summary>
        /// Output part of the rule
        /// </summary>
        TOutput Output { get; set; }

        #endregion

        /// <summary>
        /// Initializes the instance
        /// </summary>
        /// <param name="input">Input part of the rule</param>
        /// <param name="output">Output part of the rule</param>
        void Initialize(TInput input, TOutput output);
    }
}

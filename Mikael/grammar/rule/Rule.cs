﻿using Mikael.grammar.symbol;

namespace Mikael.grammar.rule
{
    /// <summary>
    /// Represents a rule
    /// </summary>
    /// <typeparam name="TRule">Type of the rule</typeparam>
    /// <typeparam name="TInput">Input type</typeparam>
    /// <typeparam name="TOutput">Output type</typeparam>
    public class Rule<TRule, TInput, TOutput, TSymbol> : IRule<TRule, TInput, TOutput, TSymbol>
        where TRule : IRule<TRule, TInput, TOutput, TSymbol>, new()
        where TInput : ISymbols<TInput, TSymbol>
        where TOutput : ISymbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        #region PROPERTIES

        /// <summary>
        /// Input part of the rule
        /// </summary>
        public TInput Input { get; set; }

        /// <summary>
        /// Output part of the rule
        /// </summary>
        public TOutput Output { get; set; }

        #endregion

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        public Rule() { }

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="input">Input part of the rule</param>
        /// <param name="output">Output part of the rule</param>
        public Rule(TInput input, TOutput output)
        {
            this.Input = input;
            this.Output = output;
        }

        /// <summary>
        /// Compares this instace with another one
        /// </summary>
        /// <param name="other">Instance to be compared against this instance</param>
        /// <returns>Result of the operation</returns>
        public int CompareTo(TRule other)
        {
            int inputC = this.Input.CompareTo(other.Input);
            if (inputC == 0)
                return this.Output.CompareTo(other.Output);
            else
                return inputC;
        }

        /// <summary>
        /// Determines if another instance is equivalent to this one
        /// </summary>
        /// <param name="other">Instance to be compared against this instance</param>
        /// <returns>True if the instances are equivalent</returns>
        public bool Equals(TRule other)
        {
            return this.Input.Equals(other.Input) && this.Output.Equals(other.Output);
        }

        /// <summary>
        /// Generates the hash code of the instace
        /// </summary>
        /// <returns>Hash code of the instace</returns>
        public override int GetHashCode()
        {
            int hash = 3;
            hash = 97 * hash + this.Input.GetHashCode();
            hash = 97 * hash + this.Output.GetHashCode();
            return hash;
        }

        /// <summary>
        /// Generates a rule
        /// </summary>
        /// <param name="input">Input part of the rule</param>
        /// <param name="output">Output part of the rule</param>
        /// <returns>Rule generated.</returns>
        public static TRule GenerateRule(TInput input, TOutput output)
        {
            TRule rule = new TRule();
            rule.Initialize(input, output);
            return rule;
        }

        /// <summary>
        /// Initializes the instance
        /// </summary>
        /// <param name="input">Input part of the rule</param>
        /// <param name="output">Output part of the rule</param>
        public virtual void Initialize(TInput input, TOutput output)
        {
            this.Input = input;
            this.Output = output;
        }
    }
}

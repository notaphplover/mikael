﻿using Mikael.grammar.symbol;

namespace Mikael.grammar.rule
{
    /// <summary>
    /// Represents a G3 Rule
    /// </summary>
    public interface IG3Rule<TRule, TOutput, TSymbol> : IRule<TRule, TSymbol, TOutput, TSymbol>
        where TRule : IG3Rule<TRule, TOutput, TSymbol>, new()
        where TOutput : IG3Symbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    { }

    /// <summary>
    /// Represents a G3 Left Rule
    /// </summary>
    public interface IG3LeftRule<TRule, TOutput, TSymbol> : IG3Rule<TRule, TOutput, TSymbol>
        where TRule : IG3Rule<TRule, TOutput, TSymbol>, new()
        where TOutput : IG3Symbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    { }

    /// <summary>
    /// Represents a G3 Right Rule
    /// </summary>
    public interface IG3RightRule<TRule, TOutput, TSymbol> : IG3Rule<TRule, TOutput, TSymbol>
        where TRule : IG3Rule<TRule, TOutput, TSymbol>, new()
        where TOutput : IG3Symbols<TOutput, TSymbol>
        where TSymbol : ISymbol<TSymbol>
    { }
}

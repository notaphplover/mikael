﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Mikael.lexical
{
    /// <summary>
    /// Represents a stream reader. This reader will be used to obtain characters and token contents.
    /// </summary>
    public class MikaelStreamReader : IMikaelStreamReader, IDisposable
    {
        /// <summary>
        /// Index of the end pointer
        /// </summary>
        protected int _endIndex;

        /// <summary>
        /// List of characters
        /// </summary>
        protected List<char> _list;

        /// <summary>
        /// Inner stream reader that will be used to obtain the chars
        /// </summary>
        protected StreamReader _reader;

        /// <summary>
        /// Index of the start pointer.
        /// </summary>
        protected int _startIndex;

        /// <summary>
        /// Creates a new instance of this class
        /// </summary>
        /// <param name="stream">Stream with the file to parse</param>
        /// <param name="encoding">Encoding used</param>
        public MikaelStreamReader(Stream stream, Encoding encoding)
        {
            _list = new List<char>();
            _reader = new StreamReader(stream, encoding);

            _startIndex = 0;
            _endIndex = 0;
        }

        /// <summary>
        /// Destructor method
        /// </summary>
        ~MikaelStreamReader()
        {
            this.Dispose();
        }

        #region ATRIBUTES

        /// <summary>
        /// Index of the end pointer
        /// </summary>
        public int EndIndex {
            get => _endIndex;
            set {
                if (value < _startIndex)
                    throw new InvalidOperationException("EndIndex must be greather than StartIndex");
                else if (value > _list.Count) {
                    this.ReadCharacters(new char[value - _endIndex], 0, value - _endIndex);
                } else {
                    _endIndex = value;
                }
            }
        }

        /// <summary>
        /// Index of the start pointer.
        /// </summary>
        public int StartIndex {
            get => _startIndex;
            set {
                if (value <= _endIndex) { 
                    if (value < 0)
                        throw new InvalidOperationException("StartIndex must not be less than zero");
                    else
                        _startIndex = value;
                } else
                    throw new InvalidOperationException("StartIndex must be greather than EndIndex");
            }
        }

        #endregion

        /// <summary>
        /// Disposes this instance
        /// 
        /// Reference: https://msdn.microsoft.com/es-es/library/b1yfkh5e(v=vs.110).aspx
        /// </summary>
        public void Dispose()
        {
            this._reader.Dispose();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Obtains the current substring of the string read based on the instance pointers
        /// </summary>
        /// <returns>Substring of the string read</returns>
        public char[] getSubstring()
        {
            int length = _endIndex - _startIndex;
            char[] characters = new char[length];
            _list.CopyTo(_startIndex, characters, 0, length);
            return characters;
        }

        /// <summary>
        /// Reads a character of the stream
        /// </summary>
        /// <param name="charRead">Character that has been read</param>
        /// <returns>Success of the operation</returns>
        public bool ReadCharacter(out char charRead)
        {
            if (_endIndex < _list.Count) {
                charRead = _list[(int)(_endIndex++)];
                return true;
            } else {
                int character = _reader.Read();
                charRead = (char)character;
                bool success = character != -1;

                if (success)
                    ++_endIndex;

                return success;
            }
        }

        /// <summary>
        /// Reads a block of characters
        /// </summary>
        /// <param name="buffer">Buffer where the block will be written.</param>
        /// <param name="index">Starting index in the buffer where the block will be written.</param>
        /// <param name="length">Maximun length of the block to read</param>
        /// <returns>Amount of characters read</returns>
        public int ReadCharacters(char[] buffer, int index, int length)
        {
            if (_endIndex < _list.Count) {
                int charactersRead = _list.Count - _endIndex;

                if (charactersRead > length)
                    charactersRead = length;

                for (int i = 0; i < charactersRead; ++i) 
                    buffer[index + i] = _list[_endIndex + i];

                _endIndex += charactersRead;

                return charactersRead + this.ReadCharacters(buffer, index + charactersRead, length - charactersRead);

            } else {
                int charactersRead = _reader.ReadBlock(buffer, index, length);

                for (int i = 0; i < charactersRead; ++i) 
                    _list.Add(buffer[index + i]);

                _endIndex += charactersRead;

                return charactersRead;
            }
        }
    }
}

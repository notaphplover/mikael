﻿namespace Mikael.lexical
{
    /// <summary>
    /// Represents a stream reader. This reader will be used to obtain characters and token contents.
    /// </summary>
    public interface IMikaelStreamReader
    {
        #region ATTRIBUTES

        /// <summary>
        /// Index of the end pointer
        /// </summary>
        int EndIndex { get; set; }

        /// <summary>
        /// Position of the start pointer
        /// </summary>
        int StartIndex { get; set; }

        #endregion

        /// <summary>
        /// Obtains the current substring of the string read based on the instance pointers
        /// </summary>
        /// <returns>Substring of the string read</returns>
        char[] getSubstring();

        /// <summary>
        /// Reads a character of the stream
        /// </summary>
        /// <param name="charRead">Character that has been read</param>
        /// <returns>Success of the operation</returns>
        bool ReadCharacter(out char charRead);
    }
}

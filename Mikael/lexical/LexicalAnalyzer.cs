﻿namespace Mikael.lexical
{
    public class LexicalAnalyzer<TReader>
        where TReader : IMikaelStreamReader
    {
        /// <summary>
        /// Stream reader used to read the input
        /// </summary>
        protected TReader _reader;

        /// <summary>
        /// Creates a new Lexical analyzer
        /// </summary>
        /// <param name="reader">Stream reader</param>
        public LexicalAnalyzer(TReader reader)
        {
            _reader = reader;
        }
    }
}

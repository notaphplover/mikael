﻿using Mikael.grammar.symbol;

namespace Mikael.lexical
{
    public interface ILexicalAnalyzer<TSymbol>
        where TSymbol : ISymbol<TSymbol>
    {
        /// <summary>
        /// Obatains the next token of the parser
        /// </summary>
        /// <returns></returns>
        TSymbol GetNextToken();
    }
}

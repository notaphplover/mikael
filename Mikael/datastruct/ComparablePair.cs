﻿using System;

namespace Mikael.datastruct
{
    public class ComparablePair<TFirst, TSecond> : Pair<TFirst, TSecond>, IComparable<ComparablePair<TFirst, TSecond>>
        where TFirst : IComparable<TFirst>
        where TSecond : IComparable<TSecond>
    {
        /// <summary>
        /// Creates a new instance of the class
        /// </summary>
        /// <param name="first">First element of the class</param>
        /// <param name="second">Second element of the class</param>
        public ComparablePair(TFirst first, TSecond second)
            : base(first, second)
        { }

        /// <summary>
        /// Compares this instance with another one
        /// </summary>
        /// <param name="other">Other instance to compare against this one</param>
        /// <returns>Result of the operation</returns>
        public int CompareTo(ComparablePair<TFirst, TSecond> other)
        {
            int comparisonResult = this.First.CompareTo(other.First);

            if (comparisonResult == 0)
                return this.Second.CompareTo(other.Second);
            else
                return comparisonResult;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Mikael.datastruct
{
    /// <summary>
    /// This is just a comparable and equatable sorted set
    /// </summary>
    public class MikaelSet<TNode, TSet> : SortedSet<TNode>, IComparable<TSet>, IEquatable<TSet>, ICloneable
        where TNode : IComparable<TNode>
        where TSet : MikaelSet<TNode, TSet>, new()
    {
        /// <summary>
        /// Create a new instance of the class
        /// </summary>
        public MikaelSet() 
            : base()
        { }

        /// <summary>
        /// Creates a new instance of the class
        /// </summary>
        /// <param name="collection">Collection with the initial elements of the set</param>
        public MikaelSet(IEnumerable<TNode> collection)
            : base(collection)
        { }

        /// <summary>
        /// Clones the instance
        /// </summary>
        /// <returns>Instance clone</returns>
        public virtual object Clone()
        {
            TSet newInstance = new TSet();
            foreach (TNode node in this)
                newInstance.Add(node);

            return newInstance;
        }

        /// <summary>
        /// Sorting algorithm
        /// </summary>
        /// <param name="other">Set to be compared against this tuple</param>
        /// <returns></returns>
        public virtual int CompareTo(TSet other)
        {
            var enumerator = this.GetEnumerator();
            var otherEnumerator = other.GetEnumerator();
            bool thisMoved;
            bool otherMoved;
            while ((thisMoved = enumerator.MoveNext()) & (otherMoved = otherEnumerator.MoveNext())) {
                int elemC = enumerator.Current.CompareTo(otherEnumerator.Current);
                if (elemC != 0)
                    return elemC;
            }

            if (thisMoved)
                return 1;
            else { 
                if (otherMoved)
                    return -1;
                else
                    return 0;
            }
        }

        public bool Equals(TSet other)
        {
            if (this.Count != other.Count)
                return false;

            foreach (TNode rule in this)
                if (!other.Contains(rule))
                    return false;

            return true;
        }
    }

    public class MikaelSet<T> : MikaelSet<T, MikaelSet<T>>
        where T : IComparable<T>
    {
        /// <summary>
        /// Create a new instance of the class
        /// </summary>
        public MikaelSet()
            : base()
        { }

        /// <summary>
        /// Creates a new instance of the class
        /// </summary>
        /// <param name="collection">Collection with the initial elements of the set</param>
        public MikaelSet(IEnumerable<T> collection)
            : base(collection)
        { }
    }
}

﻿namespace Mikael.datastruct
{
    /// <summary>
    /// Represents a pair of elements
    /// </summary>
    /// <typeparam name="TFirst">Type of the first element of the pair</typeparam>
    /// <typeparam name="TSecond">Type of the second element of the pair</typeparam>
    public class Pair<TFirst, TSecond>
    {
        /// <summary>
        /// First element of the pair
        /// </summary>
        public TFirst First { get; set; }

        /// <summary>
        /// Second element of the pair
        /// </summary>
        public TSecond Second { get; set; }

        /// <summary>
        /// Creates a new instance of the class
        /// </summary>
        /// <param name="first">First element of the class</param>
        /// <param name="second">Second element of the class</param>
        public Pair(TFirst first, TSecond second)
        {
            this.First = first;
            this.Second = second;
        }

        /// <summary>
        /// Creates a new instance of the class
        /// </summary>
        public Pair()
            : this(default(TFirst), default(TSecond))
        { }
    }

    /// <summary>
    /// Represents a pair of elements
    /// </summary>
    /// <typeparam name="T">Type of the elements</typeparam>
    public class Pair<T> : Pair<T, T>
    {
        /// <summary>
        /// Creates a new instance of the class
        /// </summary>
        /// <param name="first">First element of the class</param>
        /// <param name="second">Second element of the class</param>
        public Pair(T first, T second)
            : base(first, second)
        { }

        /// <summary>
        /// Creates a new instance of the class
        /// </summary>
        public Pair()
            : base()
        { }
    }
}
